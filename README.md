# Introduction

Les documents suivants constituent mon mémoire de maîtrise, intitulé *Quels
modèles d'affaires pour les logiciels libres? Une analyse économique du modèle
de développement particulier des logiciels libres*.

# Compiler le code

## Dépendances

La partie empirique de ce mémoire est effectuée avec le programme `R`. Sur une
plateforme Debian, vous aurez besoin des programmes suivants:

    $ sudo apt install r-base r-cran-data.table r-cran-forcats r-cran-xtable r-cran-lmtest r-cran-sandwich

## Compiler

Une fois les dépendance installées, vous pouvez compiler le code et créer les
tableaux de cette manière:

    $ cd code
    $ make

# Compiler le PDF

## Dépendances

Ce projet utilise `pandoc` pour compiler des fichiers Markdown vers un PDF à
l'aide d'un template LaTex. Sur une plateforme Debian, vous aurez donc besoin
d'installer les programmes suivants:

    $ sudo apt install pandoc pandoc-citeproc texlive-latex-base texlive-latex-extra texlive-fonts-extra texlive-xetex texlive-lang-french

## Compiler

Une fois les dépendances installées, vous pouvez compiler le mémoire en version
PDF en suivant les instructions suivantes:

    $ cd typesettings
    $ make

# Contact

Mes informations de contact sont disponibles sur mon [site web][].

[site web]: https://veronneau.org
