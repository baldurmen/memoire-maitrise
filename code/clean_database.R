#
# Clean the original database and format it
#


# Import the original database

orig_db <- fread("../database/stackoverflow_2018_results.csv")


# Remove variables we don't need

db <- orig_db[,c("Age",
                 "Gender",
                 "RaceEthnicity",
                 "Country",
                 "FormalEducation",
                 "EducationParents",
                 "Employment",
                 "ConvertedSalary",
                 "Hobby",
                 "OpenSource",
                 "YearsCoding",
                 "JobSatisfaction",
                 "CareerSatisfaction",
                 "EducationTypes",
                 "AgreeDisagree1",
                 "EthicsChoice",
                 "EthicsReport",
                 "EthicalImplications",
                 "LanguageWorkedWith",
                 "DatabaseWorkedWith",
                 "PlatformWorkedWith",
                 "IDE",
                 "OperatingSystem")]


# Age
db$Age <- match(db$Age, c("Under 18 years old", # 1
                          "18 - 24 years old",  # 2
                          "25 - 34 years old",  # 3
                          "35 - 44 years old",  # 4
                          "45 - 54 years old",  # 5
                          "55 - 64 years old",  # 6
                          "65 years or older")) # 7
db$Age0017 <- 0 ; db$Age1824 <- 0 ; db$Age2534 <- 0 ; db$Age3544 <- 0
db$Age4554 <- 0 ; db$Age5564 <- 0 ; db$Age65More <- 0
db$Age0017[db$Age==1] <- 1
db$Age1824[db$Age==2] <- 1
db$Age2534[db$Age==3] <- 1
db$Age3544[db$Age==4] <- 1
db$Age4554[db$Age==5] <- 1
db$Age5564[db$Age==6] <- 1
db$Age65More[db$Age==7] <- 1


# Gender
db$Gender <- fct_other(db$Gender, c("Male", "Female", NA), other_level = "Other") # Categorize in 3 sub-groups
db$Gender <- match(db$Gender, c("Male", "Female", "Other"))                       # Transform those categories in numerical (1 to 3)
db$Gender[db$Gender>1] <- 0                                                       # Make it a boolean (Male - not Male)
names(db)[names(db)=="Gender"] <- "GenderDummy"


# RaceEthnicityDummy
db$RaceEthnicity <- fct_other(db$RaceEthnicity, "White or of European descent",
                              other_level = "Other")
db$RaceEthnicity <- match(db$RaceEthnicity,
                           c("White or of European descent", # 1
                             "Other"))                       # 2
db$RaceEthnicity[db$RaceEthnicity==1] <- 0
db$RaceEthnicity[db$RaceEthnicity==2] <- 1
names(db)[names(db)=="RaceEthnicity"] <- "RaceEthnicityDummy"


# CountryHDI
hdi <- fread("../database/countries_HDI_2018.csv")                      # UN data
for (i in 1:length(hdi$V1)) {                                           # Replace the Country's name by its HDI
  db$Country <- replace(db$Country, db$Country==hdi$V1[i], hdi$V2[i])
}
names(db)[names(db)=="Country"] <- "CountryHDI"
db$CountryHDI <- as.numeric(db$CountryHDI)


# FormalEducationDummy
# EducationParentsDummy
for (i in 5:6) {                                                                 # Same procedure, but since FormalEducation
  db[[i]] <- fct_other(db[[i]], c("Bachelor’s degree (BA, BS, B.Eng., etc.)",    # and EducationParents have the same fields,
                                  "Master’s degree (MA, MS, M.Eng., MBA, etc.)", # we do it in a loop
                                  "Other doctoral degree (Ph.D, Ed.D., etc.)",
                                  NA),
                       other_level = "Other")
  db[[i]] <- match(db[[i]], c("Bachelor’s degree (BA, BS, B.Eng., etc.)",
                              "Master’s degree (MA, MS, M.Eng., MBA, etc.)",
                              "Other doctoral degree (Ph.D, Ed.D., etc.)",
                              "Other"))
  db[[i]][db[[i]]<4] <- 1
  db[[i]][db[[i]]==4] <- 0
}
names(db)[names(db)=="FormalEducation"] <- "FormalEducationDummy"
names(db)[names(db)=="EducationParents"] <- "EducationParentsDummy"


# EmploymentFull
# EmploymentPartSelf
# EmploymentOther
db$Employment <- fct_other(db$Employment,
                           c("Employed full-time",
                             "Employed part-time",
                             "Independent contractor, freelancer, or self-employed",
                             NA),
                           other_level = "Other")
db$Employment <- match(db$Employment,
                       c("Employed full-time",                                   # 1
                         "Employed part-time",                                   # 2
                         "Independent contractor, freelancer, or self-employed", # 3
                         "Other"))                                               # 4
db$EmploymentFull <- 0 ; db$EmploymentPartSelf <- 0 ; db$EmploymentOther <- 0
db$EmploymentFull[is.na(db$Employment)] <- NA
db$EmploymentPartSelf[is.na(db$Employment)] <- NA
db$EmploymentOther[is.na(db$Employment)] <- NA
db$EmploymentFull[db$Employment==1] <- 1
db$EmploymentPartSelf[db$Employment==2] <- 1
db$EmploymentPartSelf[db$Employment==3] <- 1
db$EmploymentOther[db$Employment==4] <- 1


# Hobby
# OpenSoure
for (i in 9:10) {
  db[[i]] <- replace(db[[i]], db[[i]]=="Yes", 1)
  db[[i]] <- replace(db[[i]], db[[i]]=="No", 0)
  db[[i]] <- as.numeric(db[[i]])
}


# More8YearsCoding
db$YearsCoding <- match(db$YearsCoding,
                        c("0-2 years",         # 1
                          "3-5 years",         # 2
                          "6-8 years",         # 3
                          "9-11 years",        # 4
                          "12-14 years",       # 5
                          "15-17 years",       # 6
                          "18-20 years",       # 7
                          "21-23 years",       # 8
                          "24-26 years",       # 9
                          "27-29 years",       # 10
                          "30 or more years"), # 11
                        nomatch=NA)
db$More8YearsCoding <- 0
db$More8YearsCoding[is.na(db$YearsCoding)] <- NA
db$More8YearsCoding[db$YearsCoding>3] <- 1


# JobSatisfied, CareerSatisfied
for (i in 12:13) {
  db[[i]] <- match(db[[i]], c("Extremely satisfied",                # 1
                              "Moderately satisfied",               # 2
                              "Slightly satisfied",                 # 3
                              "Neither satisfied nor dissatisfied", # 4
                              "Slightly dissatisfied",              # 5
                              "Moderately dissatisfied",            # 6
                              "Extremely dissatisfied"),            # 7
                   nomatch=NA)
  db[[i]][db[[i]]==2] <- 0.833
  db[[i]][db[[i]]==3] <- 0.666
  db[[i]][db[[i]]==4] <- 0.5
  db[[i]][db[[i]]==5] <- 0.333
  db[[i]][db[[i]]==6] <- 0.166
  db[[i]][db[[i]]==7] <- 0
}


# EducationTypes
db$EducationTypes <- gsub("(.*?)Contributed to open source software(.*?)", 1,
                          db$EducationTypes)
db$OpenSourceEducation <- 0
db$OpenSourceEducation[is.na(db$EducationTypes)] <- NA
db$OpenSourceEducation[db$EducationTypes==1] <- 1


# EthicsChoice
db$EthicsChoice <- match(db$EthicsChoice, c("No",                    # 1
                                            "Depends on what it is", # 2
                                            "Yes"),                  # 3
                         nomatch=NA)
db$EthicsChoice[db$EthicsChoice==2] <- 0.5
db$EthicsChoice[db$EthicsChoice==3] <- 0


# EthicsReport
db$EthicsReport <- match(db$EthicsReport, c("Yes, and publicly",                # 1
                                            "Yes, but only within the company", # 2
                                            "Depends on what it is",            # 3
                                            "No"),                              # 4
                         nomatch=NA)
db$EthicsReport[db$EthicsReport==2] <- 0.66
db$EthicsReport[db$EthicsReport==3] <- 0.33
db$EthicsReport[db$EthicsReport==4] <- 0


# EthicalImplications
db$EthicalImplications <- match(db$EthicalImplications,
                                c("Yes",                   # 1
                                  "Unsure / I don't know", # 2
                                  "No"),                   # 3
                                nomatch=NA)
db$EthicalImplications[db$EthicalImplications==2] <- 0.5
db$EthicalImplications[db$EthicalImplications==3] <- 0


# LanguageProprietary
lang <- c("(.*?)C#(.*?)",                  # We match languages that either have
          "(.*?)F#(.*?)",                  # proprietary compilers (Matlab) or that are
          "(.*?)Matlab(.*?)",              # known to be used in very proprietary
          "(.*?)Objective-C(.*?)",         # environments, like VB.NET
          "(.*?)Swift(.*?)",
          "(.*?)VB.NET(.*?)",
          "(.*?)VBA(.*?)",
          "(.*?)Visual Basic 6(.*?)")
for (i in lang) {
  db$LanguageWorkedWith <- gsub(i, "MATCH_ME", db$LanguageWorkedWith)
}
db$LanguageProprietary <- 0
db$LanguageProprietary[is.na(db$LanguageWorkedWith)] <- NA
db$LanguageProprietary[grep("MATCH_ME", db$LanguageWorkedWith)] <- 1


## DatabaseProprietary
databases <- c("SQL Server",
               "Amazon RDS/Aurora",
               "Microsoft Azure (Tables, CosmosDB, SQL, etc)",
               "Oracle",
               "IBM Db2",
               "Google Cloud Storage",
               "Amazon DynamoDB",
               "Apache HBase",
               "Apache Hive",
               "Amazon Redshift",
               "Google BigQuery")
for (i in databases) {
  db$DatabaseWorkedWith <- gsub(i, "MATCH_ME", db$DatabaseWorkedWith)
}
db$DatabaseProprietary <- 0
db$DatabaseProprietary[is.na(db$DatabaseWorkedWith)] <- NA
db$DatabaseProprietary[grep("MATCH_ME", db$DatabaseWorkedWith)] <- 1


# PlatformProprietary
platforms <- c("AWS",
               "Azure",
               "Firebase",
               "Windows Desktop or Server",
               "Heroku",
               "Amazon Echo",
               "iOS",
               "Mac OS",
               "Google Cloud Platform/App Engine",
               "SharePoint",
               "IBM Cloud or Watson",
               "Apple Watch or Apple TV",
               "Gaming console",
               "Predix",
               "Windows Phone",
               "Salesforce",
               "Google Home")
for (i in platforms) {
  db$PlatformWorkedWith <- gsub(i, "MATCH_ME", db$PlatformWorkedWith)
}
db$PlatformProprietary <- 0
db$PlatformProprietary[is.na(db$PlatformWorkedWith)] <- NA
db$PlatformProprietary[grep("MATCH_ME", db$PlatformWorkedWith)] <- 1


# IDEProprietary
ide <- c("Komodo",
         "Sublime Text",
         "Visual Studio;",
         "IntelliJ",
         "Coda",
         "Xcode",
         "PHPStorm",
         "RubyMine",
         "Zend",
         "TextMate")
for (i in ide) {
  db$IDE <- gsub(i, "MATCH_ME", db$IDE)
}
db$IDEProprietary <- 0
db$IDEProprietary[is.na(db$IDE)] <- NA
db$IDEProprietary[grep("MATCH_ME", db$IDE)] <- 1


# OSProprietary
db$OperatingSystem <- match(db$OperatingSystem, c("Linux-based", # 1
                                                  "BSD/Unix",    # 2
                                                  "Windows",     # 3
                                                  "MacOS"),      # 4
                            nomatch=NA)
db$OSProprietary <- 0
db$OSProprietary[is.na(db$OperatingSystem)] <- NA
db$OSProprietary[db$OperatingSystem>2] <- 1
