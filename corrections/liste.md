Les modifications mineures de type orthographiques ont été effectuées
directement et ne sont pas recensées ici.

# Introduction

**Commentaire 1 (R4):**

Les sections 0.1 à 0.2.4 pourraient constituer un chapitre en soi, portant sur
le contexte.

*Correction proposée:*

Faire des sections 0.1 à 0.2.4 un chapitre à part entière.

\dotfill

**Commentaire 2 (R3):**

Le terme « hacker » n'est pas un mot français et devrait être remplacé par
« pirates informatiques ».

*Correction proposée:*

Mise en italique du terme « hacker » dans le texte.

Il n'existe effectivement pas de terme en français qui capture la nuance du mot
« hacker » en anglais. Le lien entre les pirates informatiques et le mouvement
« hacker » est une idée erronée et ne fait définitivement pas honneur à la
nuance entre « hackers » et « crackers » si chère au monde informatique.

\newpage

# 1.1 Tragédie des communs

**Commentaire 3 (R4):**

Il semble plus approprié d'utiliser le terme « Tragédie des biens communs »

*Correction proposée:*

Pas de modifications prévues.

Cela est un choix stylistique (on ne souhaite pas mettre l'emphase sur le fait
que l'on parle de biens, mais plutôt parler « des communs »). De plus, le
terme « Tragédie des communs » est utilisé dans de nombreuses publications
scientifiques ainsi que pour la traduction de PUF de l'article de Hardin
<https://www.puf.com/content/La_trag%C3%A9die_des_communs>.

\newpage

# 1.2 Incitatifs économiques et logiciels libres

**Commentaire 4 (R3):**

Qu'est-ce qui peut constituer un besoin personnel? Cela n'est pas évident à
première vue.

*Correction proposée:*

Ajout d'une phrase donnant des exemples de besoins personnels.

\dotfill

**Commentaire 5 (R4):**

Il serait bénéfique de rappeler la signification du symbole « est préféré à »
utilisé à l'équation 1.1.

*Correction proposée:*

Ajout d'une phrase après l'équation 1.1 pour la résumer en mots.

\dotfill

**Commentaire 6 (R3):**

En économie, on évite de nommer le prénom des auteur-e-s que l'on cite.

*Correction proposée:*

Aucune modification prévues.

Cela est un choix stylistique. Souvent pour présenter le travail d'un-e
auteur-e, je mentionne son nom complet et par la suite j'utilise seulement
son nom de famille.

\newpage


# 2.1 Description de la base de données

**Commentaire 7 (R3, R4):**

Il manque une section de statistiques descriptives pour permettre une meilleure
compréhension de la base de données.

*Correction proposée:*

Ajout d'une section "Statistiques descriptives".

Cette section visera à mettre en évidence le travail exhaustif de présentation
de la base de données et de statistiques descriptives fait par StackOverflow,
disponible au <https://insights.stackoverflow.com/survey/2018/> et à présenter
des statistiques descriptives pour les variables agrégées, car elles ne sont
pas présentes dans l'analyse de StackOverflow.

\dotfill

**Commentaire 8 (R4):**:

Pourquoi ne pas utiliser tout les sondages depuis 2011 si ces derniers existent?

*Correction proposée:*

Ajout d'une phrase dans la description de la base de données à ce sujet.

Le fait que les questions posées dans les sondages changent à chaque année
est brièvement abordé dans la conclusion, mais visiblement ce n'est pas assez
clair.

\dotfill

**Commentaire 9 (R4)**:

Est-ce que les données contiennent de l'information sur le type d’emploi ? Par
exemple est-ce un emploi en informatique ?

*Correction proposée:*

Pas de modifications prévues.

Non, la variable "Employment" ne donne de l'information que  sur le statut
d'emploi.  Mais bon, en général toutes les personnes qui ont répondu au sondage
travaillent en informatique d'une manière ou d'une autre, de par la nature de
StackOverflow.

\dotfill

**Commentaire 10 (R4)**:

Les variables « proprietary », est-ce qu'il serait possible d’avoir trois
catégories plutôt que deux, du type « proprietary et rien d’autre »,
« les deux », et « libre/open source » seulement ?

*Correction proposée:*

Pas de modifications prévues.

Cela serait possible, mais je ne suis pas certain que cela apporterait de
l'information pertinente.

\dotfill

**Commentaire 11 (R3):**

Pourquoi le choix de 8 ans dans la variable More8YearsCoding?

*Correction proposée:*

Pas de modifications prévues.

C'est un choix arbitraire: la catégorie « 6 à 8 ans d'expérience » est la
médiane. Nous disposons de 11 catégories (YearsCoding n'est donc pas une
variable continue) et nous avons choisi de la transformer en variable
dichotomique pour éviter d'avoir à ajouter 10 variables dans nos équations
(comme nous le faisons pour l'âge).

\newpage


# 2 Analyse empirique

**Commentaire 12 (R3, R4):**

Le mot « impact » est utilisé à tord, car il n'y a pas de lien de causalité
présenté dans le travail.

*Correction proposée:*

Utilisation du mot « relation », tel que proposé par R3.

\dotfill

**Commentaire 13 (R3):**

Le nom des variables sont en anglais, alors que le mémoire est en français.

*Correction proposée:*

Pas de modifications prévues.

Les variables dans la base de données de StackOverflow sont en anglais et
j'ai choisi de ne pas les renommer en français pour éviter que cela porte à
confusion et pour que le lien avec la base de données d'origine soit plus
simple à effectuer.

\newpage


# 2.2 Méthodologie

**Commentaire 14 (R1):**

Une étude MCO est décevante, car elle ne permet pas d'établir de relations de
cause à effet. La base de données contient 129 variables et il devrait être
possible d'effectuer un Test de Durbin-Wu-Hausman.

*Correction proposée:*

Lecture des deux références proposées (Murray 2006, Nichols 2006) et ajout
d'une tentative d'identification de variables instrumentales en 2.2.7

\dotfill

**Commentaire 15 (R3):**

On mentionne qu'il manque des instruments, mais on ne dit pas lesquels. S'il
n'y a pas de variables instrumentales valides, il faudrait retirer la section
sur Durbin-Wu-Hausman et expliquer que le travail n'effectue pas d'analyse
causale.

*Correction proposée:*

Ajout d'un paragraphe dans la section sur Durbin-Wu-Hausman faisant la liste
d'instruments qu'il aurait été intéressant d'utiliser s'ils avaient existé.

\dotfill

**Commentaire 16 (R3):**

Plusieurs variables sont agrégées, mais on explique pas pourquoi un tel exercice
est nécessaire ou pertinent.

*Correction proposée:*

Ajout d'une section tentant d'expliquer la pertinence d'agréger les variables
en question.

\dotfill

**Commentaire 17 (R3, R4):**

La manière dont les variables sont agrégées est arbitraire, particulièrement à
l'équation 2.3.

*Correction proposée:*

Effectuer des recherches sur les méthodes de réduction de la dimensionnalité,
tel que proposé, pour voir s'il est possible d'utiliser des méthodes déjà
établies. <https://en.wikipedia.org/wiki/Dimensionality_reduction> semble
faire une liste intéressante de méthodes.

Sinon, comme proposé par R4, présenter un tableau où l'on fait varier les
coefficients pour voir si cela a un impact sur les résultats.

\dotfill

**Commentaire 18 (R4):**

Il est plus conventionnel de séparer la constante 1 de Xi dans les régressions.

*Correction proposée:*

Modifier les modèles pour utiliser la forme:
Yj = beta0 + betai * Xi + deltai * Di + ej

\dotfill

**Commentaire 19 (R4):**

Le terme « continue » devrait être utilisé pour décrire les variables au lieu de
« quantitatives ».

*Correction proposée:*

Utiliser « variables continues ».

\dotfill

**Commentaire 20 (R3)**:

Il serait plus juste de traiter variable CountryHDI en tant qu'effet fixe de
pays pour contrôler pour les caractéristiques fixes à l'intérieur d'un pays.

*Correction proposée:*

Pas de modifications prévues.

Les modèles à effet fixes sont normalement utilisés pour des données de panel
et je ne suis pas certain de comprendre comment effectuer ce genre travail
avec notre base de données, qui n'en contient pas.

\dotfill

**Commentaire 21 (R3, R4):**

Il serait possible de condenser les 10 pages de modèles en 1 page et d'utiliser
deux modèles plus généraux.

*Correction proposée:*

Si la manière actuelle de présenter les modèles est effectivement un peu
redondante, elle a l'avantage d'être très claire.
Pour obtenir les 10 modèles présentés, on utilise une combinaison de 8
matrices différentes (X1, X2, X3, X4, D1, D2, D3, D4). Pour simplifier les
équations, nous présenterons donc toujours 10 équations, mais en réutilisant
certaines matrices pour alléger la présentation.

\dotfill

**Commentaire 22 (R3, R4):**

L'analyse pourrait être enrichie en tentant d'observer si les résultats changent
quand on divisent les populations observées en sous-catégories.

*Correction proposée:*

Pas de modifications prévues.

Cela serait effectivement intéressant, mais complexifierait grandement
l'analyse. R3 propose par exemple de garder uniquement « les développeurs dans
des pays où la production de logiciel est forte » (cela me semble très
arbitraire, je ne connais pas de bonne manière de mesure cela), ou encore de
diviser les données par pays. Notre base de données en contient 183. Ce genre
de traitement nécessiterait un travail important pour justifier les différences
entre pays (si elles existent) et s'éloignerait de l'idée centrale du chapitre
2 qui est d'analyser les modèles présentés au chapitre 1 avec des données
récentes.

\newpage


# 2.2.7 Tests statistiques

**Commentaire 23 (R4):**

La note sur la colinéarité parfaire pour la variable EmploymentFull n'a pas de
sens, car certaines régressions ne contiennent pas de variables "Employment".

*Correction proposée:*

Effectivement, une erreur d'inattention s'est glissée dans les matrices D3,
D4, D5, D6, D7, D8, D9, D10. Elle devrait contenir les variables
EmploymentParSelf et EmploymentOther.

\dotfill

**Commentaire 24 (R4):**

Dans la section « Hétéroscédasticité », on mentionne que les « résultats »
changent. On devrait plutôt indiquer que c'est les écarts-types qui changent.

*Correction proposée:*

Remplacer « résultats » par « écarts-types » dans cette section.

\dotfill

**Commentaire 25 (R4):**

Il serait bien de mentionner la possibilité d'un biais de sélection, considérant
que les données ne sont probablement pas représentatives de l'ensemble des
développeuses ou des développeurs.

*Correction proposée:*

Ajouter une sous-section qui aborde les biais de sélection causés par la
provenance de la base de données.

\newpage


# 2.3 Analyse des résultats

**Commentaire 26 (R3, R4):**

Les effets sur les coefficients présentés ne s'interprètent pas comme des
pourcentages, mais des hausses d'index.

*Correction proposée:*

Parler d'augmentation en points de pourcentages plutôt que de pourcentages.

\newpage


# Annexe A: Quels modèles d'affaires pour financer les logiciels libres

**Commentaire 27 (R4)**:

L'annexe A devrait être intégré comme un chapitre.

*Correction proposée:*

Pas de modifications prévues.

C'est un choix qui a été fait lors de la rédaction, je n'ai pas vraiment envie
de revenir une seconde fois sur cette décision :)

\newpage


# Annexe B: Tableaux statistiques

**Commentaire 28 (R3)**:

Il faut mettre la variable d'intérêt en haut du tableau, car c'est l'élément
central.

*Correction proposée:*

Changer l'ordre des variables dans le tableau.

\dotfill

**Commentaire 29 (R3)**:

Dans les tableaux, il faudrait séparer les milliers par une virgule.

*Correction proposée:*

Ajouter des virgules pour séparer les milliers.

\dotfill

**Commentaire 30 (R3):**

2 décimales après le 0 sont suffisantes, pas besoin d'en avoir 4.

*Correction proposée:*

Modifier les tableaux pour ne garder que 2 décimales.

\dotfill

**Commentaire 31 (R3):**

Les tableaux 2.9 et 2.10 sont mentionnés avant les tableaux 2.5 à 2.8.

*Correction proposée:*

Pas de modifications prévues.

Les tableaux sont numérotés en ordre de présentation des variables à la section
2.2 et non en ordre d'apparition. Comme ils se ressemblent, choisir un ordre
arbitraire en fonction de leur présentation rendrait à mon avis les choses
moins claires.

\dotfill

**Commentaire 32 (R3):**

Le nombre d'observations n'est pas présent dans les tableaux.

*Correction proposée:*

Ajout du nombre d'observations dans les tableaux.

\dotfill

**Commentaire 33 (R3, R4):**

Tous les tableaux pourraient être regroupés en un seul puisque les variables
de contrôle ne sont pas analysées directement.

*Correction proposée:*

Pas de modifications prévues.

Regrouper nos tableaux en un seul pour ne conserver que les variables d'intérêt
(OpenSource et OpenSourceComposite) enlèverait selon nous une partie importante
de l'information présentée dans les tableaux actuels. Même si ne nous les
analysons pas directement, ces données nous semblent tout de même
intéressantes.

[modeline]: # ( vim: set spl=fr: )
