---
titlepage: true
title: Quelle structure d'incitatifs pour les logiciels libres?
subtitle: Une analyse économique du modèle de développement particulier des logiciels libres
author: Louis-Philippe Véronneau
date: janvier 2021
remerciements: |
 | Je souhaite remercier messieurs Steve Ambler et Louis Martin - respectivement
   professeur d'économie et professeur d'informatique à l'UQAM - pour avoir
   gracieusement accepté de diriger ce mémoire de maîtrise. Sans leurs judicieux
   conseils, je n'aurais pas été en mesure de terminer ce projet.
 | Merci à Delphine Labrecque-Synnott pour son amour et son support constant.
 | Merci à finalement à 陳昌倬 dont l'excellent thé vert m'a apporté force,
   concentration et vitalité tout au long de cet exercice ardu.
dedicace: |
 | Ce mémoire est dédié à Debian, dont la communauté vibrante en fait une
   distribution formidable, tant au point de vue technique qu'au point de vue
   humain. Preuve indéniable qu'une communauté saine peut s'autogérer, croître
   et perdurer, Debian aura toujours une place particulière dans mon cœur.
abreviations: |
 | **ADULLACT**: Association des développeurs et utilisateurs de logiciels libres pour les administrations et les collectivités territoriales
 | **BSD**: Berkeley Software Distribution
 | **CONTU**: Commission on New Technological Uses of Copyrighted Works
 | **FOSS**: Free and Open Source Software
 | **FSF**: Free Software Foundation
 | **GNU GPL**: GNU General Public License
 | **INRIA**: Institut national de recherche en informatique et en automatique
 | **MCO**: Moindres carrés ordinaire
 | **MIT**: Massachusetts Institute of Technology
 | **OSI**: Open Source Initiative
 | **SaaS**: Software as a Service
 | **UQAM**: Université du Québec à Montréal
abstract: |
 | Ce mémoire traite des incitatifs économiques qui motivent un individu dans
   sa décision de publier du code informatique sous une licence de propriété
   intellectuelle libre. Ce code libre a parfois une grande valeur marchande:
   pourquoi le rendre accessible gratuitement? Sujet populaire auprès de
   certain-e-s économistes dans les années 2000, peu semblent de nos jours
   s'intéresser à cette question, et ce, malgré la croissance fulgurante de
   cette branche de l'informatique dans la dernière décennie.
 | 
 | Pour y voir plus clair, nous revisitons certains modèles économiques traitant
   de ce sujet à l'aide d'une base de données récente et très populeuse, tirée
   du sondage *Stack Overflow Developers Survey 2018*. Si on constate qu'il
   est fort probable que l'attrait de la récompense monétaire soit un incitatif
   majeur à écrire du libre, d'autres incitatifs d'ordre social semblent
   également avoir un effet significatif.
keywords:
- logiciels libres
- économie
- incitatifs
- modèles d'affaires
- informatique
- \linebreak communs numériques
- open source
lang: fr
bibliography:
- ../bibliography/article.bib
- ../bibliography/book.bib
- ../bibliography/inproceedings.bib
- ../bibliography/misc.bib
- ../bibliography/techreport.bib
- ../bibliography/unpublished.bib
nocite: |
  @*
link-citations: true
tables: true
graphics: true
toc: true
lot: true
papersize: letter
documentclass: scrreprt
...

[modeline]: # ( vim: set spl=fr: )
