# Introduction {-}

L'informatique prend une place exponentielle dans nos vies. Auparavant réservés
au travail et à la sphère commerciale, les ordinateurs et autres appareils
électroniques sont dorénavant partout, à un point tel que beaucoup ne pourraient
plus envisager vivre sans eux.

Face aux multiples défis technologiques auxquels nos sociétés sont confrontées,
l'informatique libre est de plus en plus privilégiée comme moyen d'assurer à la
fois le respect de la vie privée, la sécurité informatique ainsi que
l'excellence technique d'un projet.

Plus qu'un simple mouvement marginal, les logiciels libres sont maintenant
adoptés par des entités gouvernementales partout à travers le monde et par des
géants de l'informatique tels que Google, Apple, Facebook et Amazon. Chose
difficile à envisager il y a quelques années et qui illustre bien ce changement
de paradigme, Microsoft est depuis peu l'entreprise contribuant le plus aux
logiciels libres dans le monde en terme de lignes de code [@asay2018].

Face à cette effervescence, tout porterait à croire que les logiciels libres se
portent plutôt bien. Leur popularité n'est cependant pas forcément un signe de
santé. En effet, dans les dernières années, des projets extrêmement importants
et utilisés par l'écrasante majorité de l'infrastructure du web comme *OpenSSL*
ou *GNU Privacy Guard* ont dévoilé des bogues sévères découlant d'un manque de
financement [@eghbal2016, 13; @edge2017].

La difficulté de trouver une structure pérenne pour financer adéquatement
l'écriture et la maintenance du code libre est bien réelle et semble à première
vue alimentée par la structure d'incitatifs économiques particulière aux
logiciels libres. En effet, comme le code libre est la plupart du temps
accessible gratuitement, les modèles d'affaires traditionnellement utilisés en
informatique, comme la vente de licences, ne sont pas applicables.

Quels sont les incitatifs économiques et sociaux qui poussent les développeuses
et les développeurs[^devs] à écrire du code libre?

Avant de répondre en détails à cette question, nous consacrerons le premier
chapitre de ce mémoire à un brève introduction historique et terminologique des
logiciels libres. Monde complexe et parfois arcane, cette introduction est à
notre avis nécessaire pour saisir la nature des débats économiques entourant
les modèles d'incitatifs liés aux logiciels libres. Cet exercice nous permettra
également de donner un sens aux débats portants sur les aspects éthiques de
certains modèles d'affaires qui, encore de nos jours, animent la communauté
libre.

Plongeant dans le vif du sujet, le second chapitre de ce mémoire portera sur
l'analyse de la structure d'incitatifs économiques particulière aux logiciels
libres. Nous tenterons ainsi de comprendre les raisons qui poussent certaines
personnes à offrir gratuitement du code ayant une grande valeur économique.

Si plusieurs personnes se sont déjà attardées à l'analyse théorique des
incitatifs liés aux logiciels libres, peu d'études chiffrées sur le sujet
existent. Le troisième chapitre de ce mémoire sera donc dédié à une analyse
empirique des modèles d'incitatifs économiques présentés au premier chapitre.
Nous effectuerons cette analyse à l'aide de la base de données provenant du
*Stack Overflow Developers Survey 2018*. Ce sondage donne un portrait de
l'opinion de près de 100 000 développeuses et développeurs sur différents
sujets informatiques en 2018. Nous tenterons ainsi d'établir la validité
empirique de ces modèles avec des données récentes.

Finalement, comme complément de recherche, nous présenterons succinctement en
annexe A les principaux modèles d'affaires utilisés pour financer les logiciels
libres. Le financement de l'informatique libre et les modèles d'affaires qui en
découlent ont en effet un impact important sur les possibilités de développer
du code libre tout en étant rémunéré. Plus qu'une simple question
d'administration des affaires et de plans stratégiques, le choix d'un modèle
d'affaires est rendu complexe par l'aspect éthique et politique du mouvement
libre.

[^devs]: Notre traduction du terme anglais *developers* couramment utilisé dans
         le milieu informatique pour regrouper les différentes catégories de
         professionnel-le-s qui travaillent directement avec du code
         informatique (administration système, programmation, génie logiciel,
         développement web, administration de réseaux, etc.)

[modeline]: # ( vim: set spl=fr: )
