# Qu'est-ce qu'un logiciel libre?

S'il existe plusieurs définitions de ce qui constitue un logiciel libre, la
définition la plus couramment acceptée est celle établie par Richard Stallman
et la *Free Software Foundation* (FSF) -@fsf2001 au tournant des années 2000.

Ainsi, un logiciel est considéré libre s'il intègre à sa licence de propriété
intellectuelle les quatre libertés suivantes:[^liberté]

0. La liberté d'exécuter le programme, pour tous les usages.
1. La liberté d'étudier le fonctionnement du programme et de l'adapter à ses
   besoins.
2. La liberté de redistribuer des copies du programme (ce qui implique la
   possibilité aussi bien de donner que de vendre des copies).
3. La liberté d'améliorer le programme et de distribuer ces améliorations au
   public, pour en faire profiter toute la communauté.

Ces quatre libertés sont encastrées dans des licences de propriété
intellectuelle qui décrivent en détail les conditions d'utilisation, de
modification et de redistribution des programmes libres. Plusieurs centaines de
licences libres existent, mais la grande majorité des programmes utilisent des
licences populaires comme la GNU GPL, la licence MIT ou encore la licence BSD.

La définition de la FSF --- qui est en pratique l'organisme en charge de
décider si une licence est libre ou non --- diffère légèrement de celle utilisée
par un autre organisme important, l'*Open Source Initiative* (OSI). Ainsi, il
est possible qu'une licence soit approuvée par l'OSI (et soit donc dite
*open source*), mais ne soit pas libre. En général les licences approuvées par
la FSF le sont également par l'OSI.

Si le terme *open source* est plus connu du public, nous faisons plutôt ici le
choix d'utiliser le terme « logiciel libre ». Ce terme a en effet une
connotation forte et met l'accent sur l'aspect politique et éthique de ce
mouvement plutôt que sur les modalités techniques du code informatique produit.
Les logiciels libres les plus connus à l'extérieur des communautés
informatiques sont le navigateur web *Firefox*, la plateforme de gestion de
contenu en ligne *Wordpress*, la suite de bureautique *LibreOffice* et le
lecteur multimédia *VLC*.

Comme nous le verrons dans le second chapitre, la structure d'incitatifs
économiques liés à la production de logiciels libres est complexe et est
fortement influencée par l'histoire de l'informatique libre et du militantisme
politique qui l'entoure. Avant de procéder à une analyse économique détaillée
des particularités des logiciels libres, il est ainsi important d'effectuer un
rappel historique des racines de ce mouvement.

[^liberté]: Ces quatre libertés sont numérotées de 0 à 3 pour des raisons
  historiques; jusqu'en 1990, il n'y avait que les libertés 1 à 3. Après
  réflexion, la FSF décide d'ajouter la liberté 0 avant toutes les autres pour
  marquer le fait que sans elle, les autres libertés ne peuvent s'appliquer.

## Mouvement hackers

Avec l'émergence de l'informatique comme discipline universitaire dans les
années 1960 apparaît une nouvelle génération d'étudiant-e-s à qui on donne
accès aux ordinateurs de l'université. Cette génération forme la première vague
de *hackers*, une communauté dotée d'un ethos promouvant le partage et la
curiosité --- parfois au mépris de la loi.

Le cadre juridique flou (voire inexistant) encadrant la propriété
intellectuelle du code informatique faisant fonctionner les ordinateurs ainsi
que les pratiques du milieu informatique de l'époque rendent possible la
croissance de cette communauté qui met de l'avant la coopération comme moyen de
production économique.

Les communautés *hackers* sont ainsi portées par un code d'éthique très fort qui
leur permet de maintenir une cohésion sociale importante. Steven Levy
[-@levy1984, 26---36], journaliste ayant documenté les communautés *hackers*
des années 70 et 80, résume l'ethos *hacker* en six points:

\newpage

> 1. *Access to computers---and anything which might teach you something about
the way the world works---should be unlimited and total. Always yield to the
Hands-On Imperative!*
> 2. *All information should be free.*
> 3. *Mistrust Authority---Promote Decentralization.*
> 4. *Hackers should be judged by their hacking, not bogus criteria such as
degrees, age, race or position.*
> 5. *You can create art and beauty on a computer.*
> 6. *Computers can change your life for the better.*

Plus qu'un simple code d'honneur bidon, cet ethos encadre la manière dont les
*hackers* existent et respirent. Obsédés par les promesses d'un futur
informatisé où les ordinateurs sont accessibles au commun des mortels, ces
derniers travaillent jour et nuit sur les rares machines qui leur sont
disponibles, allant même jusqu'à remplacer le cycle de vie de 24h typique par
un cycle de 30h pour gagner en efficacité. Ces *hackers* --- véritables
pionniers de l'informatique moderne --- ne sont pas motivés par le profit ou la
gloire personnelle, mais bien par le pur plaisir d'écrire du code informatique
et de réaliser le plein potentiel de leurs terminaux rudimentaires.

Levy décrit ainsi les motivations qui poussent Peter Samson, *hacker* au *MIT
Articifial Intelligence lab* dans les années 70, à écrire le tout premier
compilateur[^comp] de musique:

> *The question of royalties never came up. To Samson and the others, using the
computer was such a joy that they would have paid to do it. The fact that they
were getting paid the princely sum of $1.60 an hour to work on the computer was
a bonus. As for royalties, wasn't software more like a gift to the world,
something that was reward in itself? The idea was to make a computer more
usable, to make it more exciting to users, to make computers so interesting
that people would be tempted to play with them, explore them, and eventually
hack on them. When you wrote a fine program, you were building a community, not
churning out a product.*
>
> *Anyway, people shouldn't have to pay for software --- information should be
free!* [@levy1984, 43---44].

[^comp]: Un compilateur est un programme informatique qui transforme du code
         source --- destiné à être lu par un être humain --- en code binaire,
         destiné à être interprété par une machine.

## Institutionnalisation

En parallèle avec l'émergence du mouvement *hackers*, la classe politique
américaine profite dès 1967 des réflexions entourant la mise à jour *Copyright
Act* pour aborder les questions entourant la propriété intellectuelle du code
informatique. Après un rendez-vous manqué avec l'histoire en 1976, le Sénat
américain adopte en 1980 les recommandations de la *Commission on New
Technological Uses of Copyrighted Works* (CONTU) et encastre le code
informatique dans le *Copyright Act*.

Une décennie après le début des travaux de la CONTU, les tribunaux américains
établissent une jurisprudence forte sur cette question avec la cause *Apple v.
Franklin* de 1983, affirmant que toutes les formes de code (code source, code
binaire, code physiquement gravé dans des puces en silicone (*Read-only
memory*)) sont sujettes au *Copyright Act* américain. Ces changements
législatifs ont un impact majeur sur les communautés *hackers*, qui voient leur
modèle de production économique gravement attaqué [@véronneau2018].

> *Rare was the program that didn't borrow source code from past programs, and
yet, with a single stroke of the president's pen, the U.S. government had given
programmers and companies the legal power to forbid such reuse. Copyright also
injected a dose of formality into what had otherwise been an informal system.*
[@williams2002, 124].

Ces changements législatifs affectent durement les communautés *hackers*, qui
voient leur accès au code source des logiciels écrits par les entreprises
privées se réduire comme une peau de chagrin. La lente mort du modèle de
création de code informatique propre aux années 60 et 70 basé sur le partage et
la collaboration divise ces communautés holistiques. Partout à travers les
États-Unis, les clubs de *hackers* se fractionnent en factions rivales et se
font compétition pour les sommes faramineuses provenant de la montée en
popularité des ordinateurs personnels et des consoles de jeux vidéos
[@levy1984, 395---434].

C'est dans ce contexte maussade que Richard Stallman, *hacker* au *MIT AI lab*,
crée en 1985 la toute première licence de propriété intellectuelle libre, la
*GNU Emacs General Public Licence*. Véritable perversion du copyright, cette
licence est la première itération formelle de ce qui deviendra les 4 libertés
fondamentales des logiciels libres. De plus, elle oblige la redistribution sous
cette même licence de toutes modifications au programme, créant ainsi un effet
viral. Dans les faits, cette licence encastre l'idéal *hacker*, les pratiques
informelles de sa communauté et son ethos dans un texte légal protégé par le
système juridique américain [@williams2002, 124---132].

Stallman fonde la FSF la même année, donnant naissance au mouvement des
logiciels libres.

## Révolution *Open Source*

Blâmant Richard Stallman et la FSF pour leur approche dogmatique et leur faible
ouverture vis-à-vis le monde commercial, Eric S. Raymond, Tim O'Reilly, Bruce
Perens et une poignée d'autres figures montantes de l'informatique libre
décident de se regrouper en 1998 pour tenter de changer l'image de marque des
logiciels libres. Ces derniers désirent conserver le modèle de création
particulier des logiciels libres basé sur la collaboration et la
décentralisation, tout en laissant de côté l'aspect éthique et politique hérité
du mouvement *hacker*. C'est l'acte de fondation de l'OSI
[@dibona_ockman_stone1999].

C'est dans cette optique que ce regroupement décide d'utiliser le terme
*open source* plutôt que « logiciels libres ». En plus de briser la puissante
image de marque établie par la FSF autour des logiciels libres, le terme
*open source* a à leur avis l'avantage d'éviter l'ambiguïté apportée par le
double sens du terme *free software* en anglais [^beer]. En forgeant le terme
*open source*, l'OSI souhaite en définitive créer un terme anhistorique et
politiquement neutre:

> *The real conceptual breakthrough, though, was admitting to ourselves that
what we needed to mount was in effect a *marketing campaign*---and that it
would require marketing techniques (spin, image-building, and rebranding) to
make it work.*
>
> *[...]*
>
> *It seemed clear in retrospect that the term 'free software' had done our
movement tremendous damage over the years. [...] Most of the damage, though,
came from something worse---the strong association of the term 'free software'
with hostility to intellectual property rights, communism, and other ideas
hardly likely to endear it to an MIS manager.* [@raymond1999, 175---176]

Les efforts de l'OSI pour promouvoir les logiciels libres dans les grandes
entreprises et dans le monde commercial arrivent à point nommé. En effet, avec
la montée de l'informatique dans la sphère commerciale, certains logiciels
libres comme le serveur web *Apache* prennent littéralement leur envol. Entre la
première version publique d'*Apache* en 1995 et l'incorporation de la *Apache
Software Foundation* en 1999, le projet acquiert 61\ % des parts du marché des
serveurs webs. Le succès instantané de ce logiciel est en grande partie dû à
son modèle de développement ouvert et à sa supériorité technique [@raymond1999,
160].

*Apache* est cependant loin d'être une exception; les années 2000 sont le cadre
d'une véritable "révolution *open source*" avec la montée en force
d'entreprises spécialisées dans l'offre commerciale de logiciels libres comme
Red Hat, ou encore la consécration de *Perl*, fortement ancré dans l'écosystème
libre, comme langage de programmation du web.

[^beer]: Il n'est pas rare que la différence entre les deux significations de
         *free software* en anglais soit expliquée en déclarant: *"Think free as
         in free speech, not free beer"*.

## Massification des logiciels libres

Dans un domaine aussi changeant que l'informatique, vingt ans peuvent paraître
une éternité. Chose certaine, depuis les années 2000, notre utilisation de
l'internet et des ordinateurs a beaucoup changé. Si seulement 41\ % des ménages
américains avaient accès à l'internet chez eux en 2000, cette proportion
s'élevait à 69\ % en 2009 et à 82\ % en 2016, preuve certaine de la nécessité
grandissante d'être « connecté » de nos jours [@us_census2010; @ryan2018, 5].

Loin d'être le seul mouvement de fond ayant profondément marqué le mouvement
libre ces vingt dernières années, l'augmentation rapide de la popularité des
logiciels libres ainsi que de la quantité de code libre publié en ligne ---
phénomène que nous appellerons massification des logiciels libres --- a très
certainement eu un impact important sur l'écosystème libre dans son ensemble.

D'aucuns reconnaissent ainsi que, malgré les côtés positifs associés à la
popularité croissante des logiciels libres, leur massification s'est faite à un
certain prix. En effet, l'intérêt des grandes entreprises pour le libre s'est
accompagné d'un phénomène de cooptation contraire à l'ethos si important de
cette communauté.

Bradley M. Kuhn, *Policy Fellow* au *Software Freedom Conservancy*, un
organisme de promotion des logiciels libres, affirmait ainsi lors d'une
conférence d'informatique libre en février 2020:

> *Everyday, more FOSS is being written that has ever existed in History.
However, I feel that most for-profit companies, from my point of view, are
basically writing the wrong free software. [...] They are focused on software
that is going to increase their ability to move forward in their businesses.
They are focused on software that they can use as a component or as a base
system, open-core style, for some larger proprietary system that they are
creating. [...] In some weird sense, because of the corporate interest in FOSS,
companies have a lot more software freedom than individuals do each day.*
[@sandler_kuhn2020]

Un excellent exemple illustrant cette problématique est le système
d'exploitation pour téléphones intelligents Android, basé sur le noyau Linux.
La popularité d'Android --- plus de 2.5 milliard d'appareils étaient en service
en 2020 [@amadeo2020] --- fait ainsi de Linux le système d'exploitation le plus
populaire au monde, du moins sur papier.

En effet, si Android lui-même est libre, les téléphones intelligents
nécessitent souvent une myriade de logiciels propriétaires pour
fonctionner.[^android_nonfree] Sans des applications propriétaires comme
*Google Play*, permettant d'installer d'autres applications, ou encore *Google
Maps*, un service de cartes et de positionnement en temps réel, ces téléphones
ont en définitive peu de valeur. À quoi bon avoir un téléphone intelligent si
on ne peut pas y installer de nouvelles applications ou utiliser le GPS pour se
repérer?

Comme Google avec Android, beaucoup d'entreprises utilisent dorénavant des
logiciels libres comme base de leurs systèmes, mais se différencient par des
interfaces ou des applications propriétaires. Le résultat est un système
hybride qui ne respecte pas les quatre libertés fondamentales des logiciels
libres, même si ces systèmes ne pourraient fonctionner sans eux.

Loin d'être une panacée, la massification des logiciels libres observée depuis
deux décennies n'a donc pas toujours été un phénomène positif, particulièrement
au niveau du respect de l'ethos libre en ligne.

[^android_nonfree]: Cela n'est généralement pas le cas pour les autres
  distributions Linux.

[modeline]: # ( vim: set spl=fr: )
