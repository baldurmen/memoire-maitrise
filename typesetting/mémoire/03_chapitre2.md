# Logiciels libres, un problème d'incitatifs économiques?

## « Tragédie des communs » et protection des communs numériques

Dans le célèbre papier de Garreth Hardin intitulé *The Tragedy of the Commons*
[-@hardin1968], ce dernier avance que face à un bien public, le gain marginal
d'un individu à la surutilisation est positif, les profits étant individuels
alors que les coûts sont partagés. Éventuellement, le bien public est détruit à
force de le surutiliser.

Pour Hardin, ce qui rend ce cadre conceptuel aussi intéressant est son
caractère systémique. Le terme « tragédie » des communs est ainsi utilisé pour
illustrer l'aspect inévitable de ce phénomène. À l'instar d'une tragédie
grecque, toutes les parties concernées sont conscientes qu'un problème existe,
mais ne sont néanmoins pas en mesure de l'empêcher.

Le développement de logiciels libres semble être à première vue un cas
classique de tragédie des communs. De par leur nature ouverte, les logiciels
libres peuvent être classés comme des biens publics: n'importe qui peut ---
sans devoir payer un sou --- télécharger et utiliser un logiciel libre. Les
coûts associés à la conception et la maintenance d'un projet libre sont
cependant bien réels! Plus de 15 600 personnes différentes ont travaillé sur
le noyau Linux [^linux] entre 2005 et 2016 [@corbet_kroahhartman2017, 3],
pour un coût total estimé à plusieurs milliards de dollars [@mcpherson_al2008].

Dans un rapport produit pour la *Ford Foundation* sur les communs numériques,
Nadia Eghbal [-@eghbal2016] en arrive à la conclusion que le code libre forme
maintenant le cœur de l'infrastructure numérique contemporaine. Ces communs
numériques --- tout comme les routes et les ponts que nous empruntons
quotidiennement --- sont des biens publics essentiels sur lesquels nos sociétés
modernes sont bâties. Contrairement aux infrastructures routières, en
très grande majorité gérées et financées par les gouvernements, ces communs
numériques n'ont pas forcément de modèles de financement pérennes. Tragédie des
communs oblige, ils sont donc parfois laissés à l'abandon [@eghbal2016, 8]. Ce
manque de ressources financières peut avoir des impacts bien réels: une faille
de sécurité dans un programme libre très utilisé peut permettre à certains
acteurs malveillants de s'introduire dans des systèmes critiques et causer
d'importants dégâts.

Par exemple, en 2014 une faille est trouvée dans le logiciel libre *OpenSSL*,
utilisé par l'écrasante majorité des sites en ligne. Surnommée *Heartbleed*,
cette faille de sécurité a permis à des acteurs malveillants de voler les clefs
de chiffrement des sites utilisant *OpenSSL* et d'intercepter les
communications en ligne de centaines de milliers de personnes. Au Canada,
*Heartbleed* a rendu possible le vol de 900 numéros d'assurance sociale et a
forcé l'Agence du revenu du Canada à fermer son site plusieurs jours pendant la
période des impôts [@cbcnews2014]. Les problèmes d'*OpenSSL* ont
vraisemblablement été causés par un manque de ressources financières
[@eghbal2016, 13].

Dans des cas extrêmes, ces bogues peuvent parfois même coûter des vies. En
effet, avec la montée en puissance de l'informatique dans le domaine médical,
de plus en plus d'implants, stimulateurs cardiaques et autres appareils
médicaux utilisent du code libre. Le domaine aéronautique et l'industrie lourde
sont également régulièrement affectés par ce genre de problématique.

Pour tenter de mitiger les problèmes créés par un phénomène de tragédie des
communs, les économistes proposent traditionnellement d'établir des droits de
propriétés clairs sur la ressource surexploitée [@ostrom1990, 12]. Les quotas
de production de gaz à effet de serre et la bourse du carbone sont par exemple
des solutions très en vue pour tenter de contrer les problèmes de réchauffement
climatique. Cela n'est cependant pas une solution viable pour les logiciels
libres: ceux-ci sont par définition des biens publics et il n'est pas possible
d'en restreindre l'accès sans en faire des logiciels
propriétaires.[^common_clause]

Contrairement à d'autres ressources victimes de tragédie des communs, comme les
aquifères utilisés pour irriguer les cultures ou encore les pâturages destinés
au bétail, les logiciels libres ont l'avantage d'être des ressources
virtuelles. Le coût marginal d'utilisation d'une ressource supplémentaire d'un
logiciel libre est donc nul. Il ne serait ainsi pas à proprement dit possible
de « surutiliser » un logiciel libre. Qui plus est, les coûts de maintenance
d'un logiciel sont généralement les mêmes, qu'il soit utilisé par une seule
personne ou par dix mille individus. La particularité des projets libres est
donc qu'ils font face à des coûts de maintenance fixes et ont des coûts
variables soit extrêmement faibles, soit inexistants.

Si un financement adéquat est une condition essentielle à la réussite des
logiciels libres, il serait erroné de réduire la tragédie des communs à une
simple question d'argent. En effet, comme le fait remarquer Elinor Ostrom dans
*Governing the Commons* [-@ostrom1990] --- un ouvrage qui lui a valu en 2009 le
prix de la Banque de Suède en sciences économiques en mémoire d'Alfred Nobel
--- la tragédie des communs est d'abord et avant tout une question
d'organisation sociale.

Rejetant le terme « tragédie des communs » pour sa connotation fataliste,
Ostrom s'ancre plutôt dans un courant issu de l'économie institutionnelle pour
analyser les réussites et les échecs de la gestion collective des communs,
démontrant ainsi que la privatisation des communs n'est pas toujours une
solution souhaitable. Selon Ostrom, l'organisation collective réussit souvent là
où les problématiques de gestion sont abordées de manière holistique
[@ostrom1990, 61].

\newpage

Ostrom en arrive ainsi à identifier huit grands principes qui permettent aux
collectivités de s'organiser pour gérer adéquatement les communs qui les
entourent [@ostrom1990,  90]:

1. Il est nécessaire d'établir clairement les limites d'un projet, tant en
   termes de but final que de membres.
2. La collectivité doit avoir des règles bien adaptées aux besoins et aux
   conditions locales.
3. Il faut établir une structure de décision collective claire.
4. Il est nécessaire de mettre en place un système de surveillance,
   préférablement collectif.
5. La collectivité doit avoir un système de sanctions échelonnées et
   progressives.
6. Les mécanismes de résolution de conflits doivent être fonctionnels.
7. Les communs ne peuvent fonctionner sans la reconnaissance étatique du droit à
   l'organisation autonome.
8. Une organisation à plusieurs paliers est préférable à une organisation
   entièrement centralisée.

La gouvernance adéquate d'un projet semble donc être tout aussi importante que
le choix d'un modèle d'affaires viable pour créer des écosystèmes libres sains
et pérennes.

Malgré la pertinence du sujet, nous n'aborderons ici que le quatrième principe
pour des raisons de brièveté. Un travail d'analyse plus complet pourrait en
effet constituer à lui seul un mémoire de maîtrise des plus intéressants.
Notons cependant qu'à première vue, comme le note avec brio Shauna
Gordon-McKeon dans une conférence intitulée *Governing the Software Commons*
[-@gordon-mckeon2019], les projets libres bien établis semblent mettre en
application une majeure partie de ces principes.[^debian]

Selon le quatrième principe d'Ostrom, il est nécessaire de mettre en place un
système de surveillance pour assurer le respect des règles établies
collectivement. Typiquement, ces mécanismes ont comme but d'empêcher la
surexploitation du commun pour empêcher sa dilapidation.

Comme nous l'avons vu précédemment, les projets libres ne sont pas sujets à la
surexploitation. Les stratégies de surveillance jouent cependant un rôle très
important dans les communautés libres. En effet, de par la nature ouverte et
collaborative des logiciels libres, il n'est pas rare que de parfaits inconnus
collaborent sur un projet commun. Il est alors important d'avoir des mécanismes
institutionnels pour préserver la qualité du code informatique. Ces procédés
sont également utiles pour éviter l'ajout de code malveillant par des acteurs
hostiles, ou encore de code de mauvaise qualité pouvant entraîner des failles
de sécurité et des bogues.

Part entière du processus de maintenance du code informatique, les mécanismes
de surveillance des logiciels sont très coûteux, non seulement pour les
personnes qui les développent mais également pour les personnes qui les
utilisent. Ainsi, il n'est pas possible de savoir si la nouvelle version d'un
logiciel est réellement digne de confiance sans soumettre à un audit les
modifications au code. S'il est possible de réduire ces coûts par des
processus automatisés (intégration continue, tests unitaires, etc.), la force
de la communauté libre passe notamment par la collectivisation des procédés de
surveillance du code formant les communs libres.

Ainsi, certaines communautés comme le projet Debian ont entres autres été
bâties dans le but de mutualiser le travail de surveillance des logiciels
libres. Devenir membre à part entière du projet Debian est un processus ardu
et nécessite de réussir une série de tests techniques et éthiques pointus. Il
n'est pas rare que ce processus prenne plus d'un an à compléter. Les membres du
projet Debian jouissent donc d'une excellente réputation et sont généralement
considérés comme digne de confiance par la communauté libre.

Plutôt que d'effectuer un audit constant des logiciels qu'elle utilise, une
entreprise peut ainsi se fier sur la qualité et la sécurité des logiciels
redistribués par Debian. En retour, il est à son avantage de participer à ce
processus d'audit collaboratif: en s'assurant elle-même que certains logiciels
sont fiables dans le cadre du projet Debian, une entreprise redonne à la
communauté et contribue à la pérennité du projet. Plusieurs personnes sont
ainsi employées pour travailler sur Debian par des entreprises utilisant cette
distribution.

En définitive, la popularité grandissante des logiciels libres depuis les
années 90 témoigne de l'efficacité des mécanismes institutionnels mis en place
par la communauté libre avec le temps. Consciente de la nécessité d'une saine
gestion des communs libres, cette dernière a su contrer la « tragédie » des
communs grâce à l'application des huit principes de gestion d'Ostrom. Certes
imparfaite, la gestion collective et démocratique des communs libres a été gage
de leur succès à grande échelle, et ce, malgré l'influence d'un cadre économique
priorisant trop souvent la compétition à la collaboration.

[^linux]: Le noyau Linux est le cœur des systèmes Linux et fournit une
  interface entre les composantes physiques d'un ordinateur et les programmes
  informatiques qui souhaitent utiliser ces ressources.

[^common_clause]: Récemment, certains projets comme *MongoDB* ou encore *Redis*
  ont modifié leurs licences pour tenter d'ajouter des restrictions
  additionnelles sur l'utilisation commerciale de leurs projets. Très mal vus
  par la communauté libre, ces efforts n'ont généralement pas eu l'effet
  escompté.

[^debian]: Par exemple, le projet Debian --- véritable pilier de l'informatique
  libre depuis sa fondation en 1993 --- aborde de manière explicite les points
  1 à 6 à travers ses documents officiels, comme le *Debian Policy Manual*
  [-@debianpolicy2019].

## Incitatifs économiques et logiciels libres

Si dans la section précédente nous avons abordé la pérennité d'un modèle de
développement des logiciels libres décentralisé et démocratique, une question
reste: pourquoi une quantité non négligeable d'individus travaillent-ils
bénévolement sur des projets libres?

En effet, si certaines personnes sont payées pour développer et maintenir des
logiciels libres sur une base contractuelle ou salariée, une majeure partie de
l'écosystème libre repose sur du travail bénévole. Un sondage auprès des
membres du projet Debian en 2016 [@deblanc2017] démontre ainsi que seul 20\ %
des répondantes et des répondants sont payé-e-s pour travailler sur le projet,
et qu'en moyenne, seul 36\ % de leur travail sur cette distribution est
réellement rémunéré. Ces chiffres sont en général assez représentatifs de la
situation générale dans la communauté libre.

Et pourtant, comme le mentionne avec tant d'éloquence Eghbal [-@eghbal2016],
l'importance des logiciels libres dans l'économie contemporaine n'est plus à
démontrer. On se retrouve donc dans une situation où un nombre très important
de travailleuses et de travailleurs qualifié-e-s produisent collectivement un
ensemble de communs libres ayant une valeur marchande considérable de manière
tout à fait bénévole. À première vue, cela semble contraire à la théorie
économique standard et plus précisément aux modèles de maximisation d'utilité
par un-e agent-e économique rationnel-le. En effet, ces personnes pourraient
très bien décider d'investir leur temps dans un projet informatique rémunéré,
le marché du travail en informatique étant notoire pour sa compétitivité.

Force est donc de constater qu'il existe une structure d'incitatifs économiques
particulière aux logiciels libres. Avec les années, plusieurs pistes de
solutions pour tenter de comprendre ce phénomène ont été proposées par des
économistes. Dans cette section, nous souhaitons présenter ces différents
modèles.

### Lerner et Tirole: une affaire de réputation

Une des explications les plus courantes du phénomène particulier d'incitatifs
économiques propres aux logiciels libres est celui de la réputation. Reprise en
2001 par MM. Lerner et Tirole dans un papier très populaire, cette théorie
soutient que la participation à un projet libre agit en quelque sorte comme une
carte de visite pour les développeuses et les développeurs.

Très visible, beaucoup utilisé par la communauté et facilement consultable, le
code libre produit par un individu devient ainsi une bonne manière de se
distinguer pour obtenir une promotion, ou encore de démontrer de manière très
concrète ses capacités à un futur employeur. En effet, les questions de
licences et de secret commercial empêchent bien souvent une personne à la
recherche d'un emploi de partager le travail qu'elle a effectué sur des
logiciels propriétaires pour démontrer son mérite.

Lerner et Tirole argumentent cependant que malgré son importance, l'attrait de
la réputation est trop faible pour expliquer le succès des logiciels libres et
reconnaissent ainsi un rôle (mineur) à l'altruisme dans le développement des
logiciels libres.

Ces derniers mentionnent finalement que beaucoup d'entreprises, sans avoir de
politiques claires sur l'utilisation de projets libres, finissent en général
par utiliser une part non négligeable de logiciels libres à l'interne. Cela se
fait souvent à l'insu des cadres supérieurs, mais avec la bénédiction des
directions intermédiaires. Par la force des choses, une partie des employé-e-s
utilisant ces projets finirait par contribuer en retour. Lerner et Tirole
parlent de ce phénomène comme d'une « subvention involontaire » aux projets
libres, mais peinent à comprendre que ces compagnies gagnent beaucoup à
utiliser ces projets libres plutôt que des logiciels propriétaires.  À notre
avis, le mot « subvention » n'est donc pas adéquat et il vaudrait mieux parler
de « participation involontaire ».

Malgré le succès de leur papier, ces derniers ne présentent pas de modèle
économique concret pour étayer leurs réflexions. Nous retenons cependant que si
leurs hypothèses  sur la réputation sont vraies, il devrait exister une
corrélation positive entre le salaire et la participation à des projets libres.

\newpage

### Wu, Gerlach & Young: incitatifs et économie comportementale

Dans un papier publié en 2007, Wu, Gerlach & Young cherchent à en savoir plus
sur ce qui motive les personnes qui développent des logiciels libres.

En se basant sur les travaux de Lerner et Tirole précédemment mentionnés, ces
derniers développent un modèle théorique d'incitatifs inspiré de
l'*Expectancy-Value Theory*, une variante de la théorie du choix rationnel
utilisée en économie comportementale, et sondent 148 personnes pour le mettre à
l'épreuve.

Ils avancent ainsi que les incitatifs principaux poussant les individus à
s'adonner aux projets libres sont:

1. la motivation intrinsèque d'aider
2. le désir d'améliorer son capital humain
3. la perspective d'obtenir un meilleur emploi
4. les besoins personnels[^besoin]

De surcroît, les auteurs divisent ces incitatifs entre incitatifs intrinsèques
(1) incitatifs extrinsèques (2, 3 et 4).

Les résultats de leur sondage démontre ainsi que:

\begin{equation}
\label{eq:wu_gerlach_young_pref}
1 \succ 2 \sim 3 \succ 4
\end{equation}

En langage courant, l'équation \ref{eq:wu_gerlach_young_pref} indique ainsi que
la motivation intrinsèque d'aider tend à être la raison la plus souvent citée,
suivie ex æquo par le désir d'améliorer son capital humain et la perspective
d'obtenir un meilleur emploi. Finalement, combler un besoin personnel serait la
raison la moins populaire pour écrire du code libre.

Le faible nombre de répondant-e-s au sondage effectué ne permet pas à notre
avis de tirer de conclusions claires de cette étude, le risque de biais de
sélection des participant-e-s étant relativement élevé. La distinction entre
incitatifs intrinsèques et extrinsèques nous semble toutefois intéressante.

[^besoin]: L'auteur de ce mémoire est par exemple le fier créateur de
  `rename-flac`, un programme libre très simple lui permettant d'ordonner
  automatiquement sa collection de musique numérique, une tâche manuelle
  auparavant bien pénible.

### Benkler: de l'importance des incitatifs sociaux

Yochai Benkler --- spécialiste de l'économie des réseaux et professeur à
l'université Harvard --- se penche sur les questions reliées aux incitatifs
économiques dans un papier intitulé *Coases's Penguin, or, Linux and the Nature
of the Firm*, paru en 2002.

Partant de l'analyse de Ronald Coase sur la place des entreprises dans
l'économie [-@coase1937], Benkler argumente que le travail collaboratif
(*peer-production*) --- une forme de travail endémique aux logiciels libres ---
est un nouveau modèle de production économique distinct des modèles
traditionnels.

Ce dernier soutient de ce fait que le travail coopératif en informatique libre,
contrairement au milieu académique, a la particularité de mettre en commun des
contributions provenant d'horizons très différents. Ainsi, il n'est pas rare
que se côtoient dans un même projet libre des employé-e-s de grandes
multinationales en informatique, des personnes travaillant pour des organismes
à but non lucratifs et des individus non rémunérés travaillant sur le projet de
manière bénévole. Comme beaucoup [@mustonen2003; @lerner_tirole2001;
@broca2013], Benkler affirme que c'est l'aspect décentralisé et ouvert des
projets libres qui réduit le coût d'opportunité de participer à un projet libre
et qui permet l'existence de cette structure particulière.

Se penchant sur la question des incitatifs économiques liés à la production de
logiciels libres, Benkler affirme que le modèle économique néoclassique, où
l'utilité est réduite à un équivalent monétaire, ne permet pas d'expliquer ce
phénomène convenablement. Il propose ainsi un modèle prenant en compte les trois
aspect suivants:

1. l'utilité de la récompense monétaire
2. l'utilité de la récompense hédoniste intrinsèque
3. l'utilité de la récompense socio-psychologique

En reprenant la classification de Wu, Gerlach & Young, on peut ainsi classer
les récompenses monétaires et socio-psychologiques comme des incitatifs
extrinsèques et la récompense hédoniste intrinsèque comme un incitatif
intrinsèque au développement de logiciels libres. Ce modèle est à notre avis
bien plus complet que celui avancé par Lerner et Tirole, qui ne considère que
la réputation comme un gain monétaire différé.

De manière plus formelle, on peut représenter la proposition de Benkler sous la
forme d'une fonction d'utilité espérée de type Bernoulli, où $U$ est l'utilité,
$M$ la récompense monétaire, $H$ la récompense hédoniste intrinsèque et $S$ la
récompense socio-psychologique. On obtient ainsi l'équation
\ref{eq:benkler_utility}.

\begin{equation}
\label{eq:benkler_utility}
U \sim f(M, H, S)
\end{equation}

### Rossi & Coleman: hétérogénéité des pratiques

Dans le cadre d'une revue étoffée des études abordant les problèmes économiques
entourant l'informatique libre, Maria Alexandra Rossi [-@rossi2004] conclut que
les incitatifs économiques liés aux logiciels libres sont très hétérogènes.
Reprenant elle aussi la distinction entre facteurs extrinsèques et
intrinsèques, elle affirme que de par leur nature décentralisée et ouverte, les
projets libres sont eux-mêmes très hétérogènes.

À son avis, cette hétérogénéité rend parfois difficile la comparaison des
logiciels libres ayant une base institutionnelle forte (le noyau Linux, les
projets de la fondation *Apache*, etc.) avec ceux développés sur une base moins
formelle (projets individuels, scripts ouverts, etc.).

Les conclusions de Rossi rejoignent les propos de Gabriella Coleman,
anthropologue spécialiste des relations sociales dans le milieu des logiciels
libres et professeure à l'université McGill. D'avis que la grande hétérogénéité
des logiciels libres est rarement comprise par les économistes, Coleman argue
que tenter de restreindre un phénomène éminemment social à sa seule
manifestation économique est une erreur:

> *Although a number of these studies tangentially discuss ethical questions
(e.g. conflict resolution within F/OSS projects), they rarely address how
developers commit themselves to an ethical vision through, rather than prior
to, their participation in a F/OSS project. Much of the F/OSS literature, in
other words, is heavily focused on the question of motivation or incentive
mechanisms, and often fails to account for the plasticity of human motivations
and ethical perceptions.* --- [@coleman2013, 123]

Pris ensemble, ces quatre visions des incitatifs économiques liés aux logiciels
libres jouent à notre avis un rôle complémentaire et se renforcent mutuellement.
Phénomène hétérogène ancré dans la diversité des individus et des communautés
qui la compose, le monde des logiciels libres a très clairement à la fois un
aspect économique et un aspect social.

De plus, la distinction entre incitatifs intrinsèques et extrinsèques nous
semble très appropriée, apportant à la fois structure et clarté à un fait
social parfois difficile à comprendre de par ses frontières floues. Grâce aux
études abordées dans cette section, nous avons identifié deux incitatifs
extrinsèques et deux incitatifs intrinsèques nous semblant jouer un rôle
central dans la production de code informatique libre. Respectivement, ces
derniers sont le gain monétaire espéré par l'écriture de code libre, le
sentiment d'appartenance à la communauté libre, le plaisir d'écrire du code
informatique libre et la gratification altruiste d'offrir du code libre.

Nous reviendrons à ces incitatifs dans le troisième chapitre de ce mémoire, où
nous tenterons de valider empiriquement leur existence.

[modeline]: # ( vim: set spl=fr: )
