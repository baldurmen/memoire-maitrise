# Analyse empirique des incitatifs économiques liés aux logiciels libres

Dans le second chapitre de ce mémoire, nous avons effectué une analyse des
différentes études portant sur les incitatifs économiques liés aux logiciels
libres. Nous avons ainsi pu voir que malgré la grande hétérogénéité des facteurs
poussant les individus à travailler sur des projets libres, il est possible de
catégoriser les incitatifs économiques ayant une influence notable entre
intrinsèques et extrinsèques.

Si plusieurs études précédemment mentionnées incluent des analyses empiriques
pour appuyer les modèles développés, les données utilisées sont souvent assez
anciennes et ont un échantillonnage relativement faible. Dans ce chapitre, nous
proposons donc d'analyser ces modèles avec une base de données récente et ayant
un large échantillonnage, dans le but de les valider ou les invalider
empiriquement.

Plus précisément, nous commencerons par regarder si la participation à des
projets libres a un effet sur le salaire annuel des développeuses et des
développeurs, comme l'affirment @lerner_tirole2001, @mustonen2003 et bien
d'autres. Nous regarderons par la suite l'effet de la participation à des
projets libres sur le plaisir personnel résultant de l'écriture de code
informatique.

Nous procéderons finalement à l'analyse de divers facteurs comme le sentiment
d'appartenance à la communauté informatique, le désir d'écrire du code pour des
raisons altruistes et la satisfaction au travail.

## Description de la base de données

Stack Overflow est un site web très populaire chez les développeurs et les
développeuses qui permet de poser des questions en lien avec la programmation
informatique et de recevoir des réponses et des conseils de la part des autres
membres de la communauté. Les personnes utilisant le site peuvent voter pour
les meilleures réponses, ce qui crée un classement très efficace. En juin 2019,
le site se classait au 47^e^ rang des sites les plus consultés en ligne selon
la compagnie *Alexa Internet, Inc.* -@alexa2019, qui se spécialise dans ce
genre de palmarès.

À toutes les années depuis 2011, Stack Overflow demande aux membres de sa
communauté de remplir un sondage en ligne comportant plus d'une centaine de
questions portant sur divers aspects de la profession informatique. Par exemple,
les répondant-e-s sont sondé-e-s sur leur participation à des projets libres,
sur les technologies utilisées dans le cadre de leur travail, sur leur salaire,
sur leurs perspectives de carrière ou encore sur le nombre et la taille de leurs
écrans au travail.

Stack Overflow effectue ce sondage annuel pour promouvoir son image de marque et
comme moyen de collecter une panoplie de données sur ses utilisatrices et
utilisateurs. Ces données peuvent par la suite être utilisées à l'interne pour
améliorer leurs services commerciaux, notamment leur service de recrutement
professionnel.

Il est intéressant de noter qu'avec le temps, à l'instar du *CEO Confidence
Survey* produit par le *Conference Board* américain, ce sondage est devenu une
véritable institution de la communauté informatique. Ces données sont ainsi
fréquemment reprises par des médias spécialisés pour analyser l'évolution des
tendances lourdes dans l'industrie [@bright2018; @bright2019].

Les réponses à ce sondage sont mises en ligne et accessibles sous la licence
libre *Open Database License*. Nous avons choisi de travailler avec les données
issues du sondage effectué en 2018 [@stackoverflow2018] car les questions
posées abordent des sujets connexes à notre sujet d'étude.

Le sondage a été effectué entre le 8 et le 28 janvier 2018, soit sur une
période de 20 jours. Comme la base de donnée provient de ce sondage ponctuel et
n'est pas le résultat d'une étude longitudinale, les réponses sont donc un
portrait de l'opinion des répondant-e-s à cette date. Conséquemment, il ne nous
est pas non plus possible d'utiliser les données de plusieurs sondages pour
analyser l'évolution des réponses dans le temps.

Plus de cent mille développeuses et développeurs informatiques provenant de 183
pays différents ont répondu à ce sondage. Les réponses de 20 000 autres
répondant-e-s n'ont pas été conservées par le service statistique de Stack
Overflow, car elles et ils n'ont pas répondu à assez de questions pour que leur
questionnaire soit considéré valide. La validité d'un questionnaire a été
établie à partir du ratio entre le temps de réponse et le nombre de questions
remplies. La plupart des questionnaires remplis en moins de 5 minutes n'ont pas
été conservés. Il n'était pas possible pour une même personne de répondre au
sondage plusieurs fois.

Sur les 101 592 personnes ayant des questionnaires considérés comme valides,
67 441 (66,4\ %) ont complété le sondage dans son entièreté.

Les répondant-e-s ont été recruté-e-s en ligne. Les cinq moyens les plus
efficaces pour recruter des répondant-e-s se sont avérés être:

1. L'achat de bannières publicitaires sur le web
2. La publication de messages sur des listes courriels
3. L'affichage de publicités sur le site stackoverflow.com
4. La publication d'entrées de blogue sur le site stackoverflow.blog
5. La publication de messages sur Twitter

Stack Overflow reconnaît que ses méthodes de recrutement des répondant-e-s ont
favorisé la participation des membres les plus actifs de sa communauté, au
détriment des personnes n'utilisant que rarement le site. De plus, les
répondant-e-s qui ont terminé le sondage se sont vu attribuer un badge spécial
dans leur profil Stack Overflow faisant mention dudit sondage.

Il est ainsi probable qu'il existe un biais de sélection dans les données
finales, causé par une différence entre la population étant portée à répondre
au sondage annuel de Stack Overflow et la population regroupant l'ensemble des
développeuses et des développeurs.  À notre avis, le nombre important de
répondant-e-s tend toutefois à minimiser le risque d'un biais marqué.

À notre connaissance, aucun travail académique en économie n'a été réalisé à
partir du sondage de 2018. Des études portant sur la participation des femmes
en informatique ont toutefois utilisé les données de sondages précédents
[@ford2016; @ford2017].

Stack Overflow met à notre disposition un total de 129 variables différentes
après avoir retiré celles comportant des informations permettant d'identifier
un individu. Sur ces 129 variables, seules les variables présentées à la
table \ref{table:var_desc} ont été retenues.

Nous avons retenu ces variables pour leur lien spécifique avec notre sujet
d'étude. La liste complète des variables de la base de données originelle ainsi
que leur description est disponible en ligne sur le site de présentation de la
base de données [@stackoverflow2018].

\input{../tables/var_desc.tex}

## Méthodologie

À l'aide de base de données, nous effectuons une série de régressions à l'aide
d'un estimateur de type « Moindres carrés ordinaire » (MCO). Plus précisément,
nous souhaitons estimer l'effet de la participation à un projet libre sur:

1. le salaire
2. le plaisir ressenti à écrire du code informatique
3. le sentiment d'appartenance à la communauté informatique
4. l'altruisme
5. la satisfaction au travail

Pour des raisons techniques, nous modifions certaines variables provenant de la
base de données originelle. En effet, plusieurs d'entre elles sont des variables
catégoriques que nous traiterons plutôt comme des variables dichotomiques. Ces
modifications sont présentées à la table \ref{table:var_desc_new}.

\input{../tables/var_desc_new.tex}

La variable `CountryHDI` est obtenue en croisant la variable `Country` avec
l'indice de développement humain du pays en 2018 [@united_nations2019]. Cela
nous permet en effet d'obtenir de l'information sur le niveau de développement
du pays de résidence du répondant ou de la répondante lors de l'année du
sondage.

Pour étudier l'appartenance à la communauté informatique, nous renommons la
variable `AgreeDisagree1` par `Belonging` et la transformons en variable
continue allant de 0 à 1. Ainsi, plus les répondant-e-s éprouvent un lien
d’appartenance fort à la communauté, plus la variable `Belonging` s'approche de
1.

L'altruisme, de par sa nature morale et philosophique, est une chose complexe à
quantifier. Comme il ne nous est malheureusement pas possible de mesurer cette
variable directement à partir de notre base de données, nous avons choisi de
retenir les variables `EthicsChoice`, `EthicsReport` et `EthicalImplications`
comme indicateurs d'altruisme.

En effet, il y a à notre avis un lien fort entre le refus d'écrire du code
informatique pour des raisons morales et le niveau d'altruisme chez une
personne. En général, lorsqu'une personne refuse d'écrire du code qu'elle juge
immoral, c'est parce qu'elle se préoccupe des répercutions possibles sur ses
pairs et sur la société en général.

On forme donc la variable `Altruism`, allant de 0 à 1, pour étudier le niveau
d'altruisme des individus sondés, où la valeur de la variable est donnée par
l'équation \ref{eq:var_alt}.

\begin{equation}
  \label{eq:var_alt}
  Altruism = \frac{EthicsChoice + EthicsReport + EthicalImplications}{3}
\end{equation}

Ainsi, si une personne refuserait d'écrire du code informatique à des fins
jugées profondément immorales, si elle dénoncerait publiquement le fait de se
faire demander une telle chose et qu'elle pense avoir l'obligation d'analyser
les conséquences morales d'écrire du code informatique, sa variable `Altruism`
serait de 1.

Contrairement à l'altruisme, la satisfaction au travail est nettement plus
simple à évaluer; on dispose en effet de deux variables appropriées,
`JobSatisfaction` et `CareerSatisfaction`. On agrège de fait ces  deux
variables via l'équation \ref{eq:var_sat}. La variable résultante,
`Satisfaction`, va de 0 à 1: plus une personne est satisfaite au travail plus
son score se rapproche de 1.

\begin{equation}
  \label{eq:var_sat}
  Satisfaction = \frac{JobSatisfaction + CareerSatisfaction}{2}
\end{equation}

\newpage

Finalement, on crée la variable `OpenSourceComposite` pour tenter de quantifier
le degré de participation à la communauté libre d'un répondant ou d'une
répondante. Cette variable composite est continue allant de 0 à 1 et est
définie par l'équation \ref{eq:var_os_c}, où:

* `OpenSourceComposite` $= OS_C$
* `OpenSource` $= OS$
* `OpenSourceEducation` $= OS_E$
* `LanguageProprietary` $= L_P$
* `DatabaseProprietary` $= D_P$
* `PlatformProprietary` $= P_P$
* `IDEProprietary` $= I_P$
* `OSProprietary` $= O_P$

\begin{alignat}{2}
  \label{eq:var_os_c}
  && OS_C &= \frac{1}{3} \cdot OS + \frac{1}{9} \cdot (OS_E + \neg L_P + \neg D_P + \neg P_P + \neg I_P + \neg O_P) \notag \\
  &&&= \frac{1}{3} \cdot OS + \frac{1}{9} \cdot (OS_E + 1 - L_P + 1 - D_P + 1 - P_P + 1 - I_P + 1 - O_P) \notag \\
  &&&= \frac{1}{3} \cdot OS + \frac{1}{9} \cdot OS_E - \frac{1}{9} \cdot ( L_P + D_P +  P_P +  I_P +  O_P) + \frac{5}{9}
\end{alignat}

Par exemple, si une personne participe à des projets libres, a déjà participé à
un projet libre à des fins éducatives, programme avec un langage dont le
compilateur est libre, travaille avec des bases de données libres, utilise un
environnement de programmation libre ainsi que Linux, sa variable
`OpenSourceComposite` serait de 1. À l'inverse, une personne affirmant ne pas
participer à la communauté libre et utilisant exclusivement des technologies
propriétaires aurait un score de 0.

Dans l'équation \ref{eq:var_os_c}, nous attribuons des poids différents aux
variables qui composent `OpenSourceComposite`. Si le rapport entre la variable
`OpenSource` ($\sfrac{1}{3}$) et les autres variables utilisées
($\sfrac{1}{9}$) est en définitive arbitraire, il est à notre avis important
d'attribuer plus de poids à `OpenSource` pour mettre l'accent sur son
importance. En définitive, une développeuse ou un développeur qui utilise des
logiciels libres mais qui affirme ne pas contribuer à ces derniers ne participe
pas réellement à cette communauté.[^contribution]

[^contribution]: Contrairement à ce qu'une personne n'évoluant pas dans la
  communauté libre pourrait être portée à croire, il n'est pas nécessaire
  d'écrire du code informatique ou même de savoir coder pour contribuer aux
  logiciels libres. En effet, de nombreuses formes de contributions non
  techniques sont nécessaires à la pérennité d'un projet. Par exemple, une
  personne peut soumettre un rapport de bogue en cas de problèmes, participer à
  la traduction de la documentation ou de l'interface graphique d'un programme
  ou encore participer à la gestion des affaires administratives d'un projet.

### Statistiques descriptives

À tous les ans, Stack Overflow [-@stackoverflow2018] rend disponible en ligne
un rapport statistique détaillé préparé par son équipe de statisticien-ne-s
pour accompagner la publication des données de son sondage annuel.

Nous encourageons le lecteur ou la lectrice intéressé-e par la distribution
statistique des variables utilisées dans ce travail à consulter ce rapport;
interactif et très détaillé, il saura mettre en valeur notre base de données
bien mieux que ce nous aurions pu présenter ici.

Comme les variables dichotomiques agrégées présentées précédemment ne font bien
évidemment pas partie des statiques descriptives publiées par Stack Overflow,
nous présentons ces données à la table \ref{table:aggregates}.

### Le salaire

On souhaite vérifier l'effet de la participation à un projet libre sur le
salaire de l'individu. On pose ainsi l'équation \ref{eq:sal}.

\begin{equation}
  \label{eq:sal}
  ConvertedSalary = \beta_0 + \beta X_1 + \delta D_1 + \epsilon
\end{equation}

$\epsilon$ est un vecteur de termes d'erreur. Les matrices lignes $\beta$
et $\delta$ contiennent respectivement les vecteurs de coefficient de
régression des vecteurs colonnes $X_1$ et $D_1$. On définit ces derniers ainsi:

\newpage

\begin{equation}
  X_1
  =
  \begin{bmatrix}
    CountryHDI
  \end{bmatrix}
\end{equation}

\begin{equation}
  D_1
  =
  \begin{bmatrix}
    OpenSource \notag \\
    Age0017 \\
    Age1824 \\
    Age3544 \\
    Age4554 \\
    Age5564 \\
    Age65More \\
    GenderDummy \\
    RaceEthnicityDummy \\
    FormalEducationDummy \\
    EducationParentsDummy \\
    EmploymentPartSelf \\
    EmploymentOther \\
    More8YearsCoding \\
    Hobby
  \end{bmatrix}
\end{equation}

Les termes de la matrice $X_1$ sont des variables continues, alors que ceux
de la matrice $D_1$ sont des variables dichotomiques.

De manière similaire, on calcule l'effet de la variable continue
`OpenSourceComposite` sur le salaire par l'équation \ref{eq:sal_c}.

\begin{equation}
  \label{eq:sal_c}
  ConvertedSalary_c = \beta_0 + \beta X_2 + \delta D_2 + \epsilon
\end{equation}

\begin{equation}
  X_2
  =
  \begin{bmatrix}
    OpenSourceComposite \\
    CountryHDI
  \end{bmatrix}
\end{equation}

\begin{equation}
  D_2
  =
  \begin{bmatrix}
    Age0017 \notag \\
    Age1824 \\
    Age3544 \\
    Age4554 \\
    Age5564 \\
    Age65More \\
    GenderDummy \\
    RaceEthnicityDummy \\
    FormalEducationDummy \\
    EducationParentsDummy \\
    EmploymentPartSelf \\
    EmploymentOther \\
    More8YearsCoding \\
    Hobby
  \end{bmatrix}
\end{equation}

Les résultats des équations \ref{eq:sal} et \ref{eq:sal_c} sont respectivement
présentés aux tableaux \ref{table:reg_sal} et \ref{table:reg_sal_c}.

### Le plaisir d'écrire du code informatique

De manière similaire, on cherche à établir l'effet de la participation à un
projet libre sur le plaisir d'écrire du code informatique, représenté par la
variable `Hobby`. On prend ainsi pour acquis que si une personne écrit du code
informatique comme hobby, elle tire de cette activité un certain plaisir. On
pose ainsi l'équation \ref{eq:hob}.

\begin{equation}
  \label{eq:hob}
  Hobby = \beta_0 + \beta X_3 + \delta D_3 + \epsilon
\end{equation}

\begin{equation}
  X_3
  =
  \begin{bmatrix}
    CountryHDI \\
    ConvertedSalary
  \end{bmatrix}
\end{equation}

\begin{equation}
  D_3
  =
  \begin{bmatrix}
    OpenSource \notag \\
    Age0017 \\
    Age1824 \\
    Age3544 \\
    Age4554 \\
    Age5564 \\
    Age65More \\
    GenderDummy \\
    RaceEthnicityDummy \\
    FormalEducationDummy \\
    EducationParentsDummy \\
    EmploymentPartSelf \\
    EmploymentOther \\
    More8YearsCoding
  \end{bmatrix}
\end{equation}

De manière similaire, on calcule l'effet de la variable continue
`OpenSourceComposite` sur le plaisir d'écrire du code informatique par
l'équation \ref{eq:hob_c}.

\begin{equation}
  \label{eq:hob_c}
  Hobby_c = \beta_0 + \beta X_4 + \delta D_4 + \epsilon
\end{equation}

\begin{equation}
  X_4
  =
  \begin{bmatrix}
    OpenSourceComposite \\
    CountryHDI \\
    ConvertedSalary
  \end{bmatrix}
\end{equation}

\begin{equation}
  D_4
  =
  \begin{bmatrix}
    Age0017 \notag \\
    Age1824 \\
    Age3544 \\
    Age4554 \\
    Age5564 \\
    Age65More \\
    GenderDummy \\
    RaceEthnicityDummy \\
    FormalEducationDummy \\
    EducationParentsDummy \\
    EmploymentPartSelf \\
    EmploymentOther \\
    More8YearsCoding
  \end{bmatrix}
\end{equation}

Les résultats des équations \ref{eq:hob} et \ref{eq:hob_c} sont respectivement
présentés aux tableaux \ref{table:reg_hob} et \ref{table:reg_hob_c}.

### Le sentiment d'appartenance

On souhaite maintenant établir l'effet de la participation à un projet libre
sur le sentiment d'appartenance à la communauté informatique. On pose ainsi
l'équation \ref{eq:bel}.

\begin{equation}
  \label{eq:bel}
  Belonging = \beta_0 + \beta X_3 + \delta D_1 + \epsilon
\end{equation}

De manière similaire, on calcule l'effet de la variable continue
`OpenSourceComposite` sur ce même sentiment d'appartenance par l'équation
\ref{eq:bel_c}.

\begin{equation}
  \label{eq:bel_c}
  Belonging_c = \beta_0 + \beta X_4 + \delta D_2 + \epsilon
\end{equation}

Les résultats des équations \ref{eq:bel} et \ref{eq:bel_c} sont respectivement
présentés aux tableaux \ref{table:reg_bel} et \ref{table:reg_bel_c}.

### L'altruisme

On souhaite établir l'effet de la participation à un projet libre sur
l'altruisme des personnes sondées. On pose ainsi l'équation \ref{eq:alt}.

\begin{equation}
  \label{eq:alt}
  Altruism = \beta_0 + \beta X_3 + \delta D_1 + \epsilon
\end{equation}

De manière similaire, on calcule l'effet de la variable continue
`OpenSourceComposite` sur l'altruisme grâce à l'équation \ref{eq:alt_c}.

\begin{equation}
  \label{eq:alt_c}
  Altruism_c = \beta_0 + \beta X_4 + \delta D_2 + \epsilon
\end{equation}

Les résultats des équations \ref{eq:alt} et \ref{eq:alt_c} sont respectivement
présentés aux tableaux \ref{table:reg_alt} et \ref{table:reg_alt_c}.

### La satisfaction au travail

On souhaite établir l'effet de la participation à un projet libre sur la
satisfaction au travail des personnes sondées. On pose ainsi l'équation
\ref{eq:sat}.

\begin{equation}
  \label{eq:sat}
  Satisfaction = \beta_0 + \beta X_3 + \delta D_1 + \epsilon
\end{equation}

De manière similaire, on calcule l'effet de la variable continue
`OpenSourceComposite` sur la satisfaction au travail grâce à l'équation
\ref{eq:sat_c}.

\begin{equation}
  \label{eq:sat_c}
  Satisfaction_c = \beta_0 + \beta X_4 + \delta D_2 + \epsilon
\end{equation}

Les résultats des équations \ref{eq:sat} et \ref{eq:sat_c} sont respectivement
présentés aux tableaux \ref{table:reg_sat} et \ref{table:reg_sat_c}.

### Choix des unités de mesure

Dans le cadre de ce travail, nous avons choisi d'effectuer des régressions en
niveau plutôt que de procéder à une transformation logarithmique. Si on aurait
pu s'attendre à ce que ce genre de transformation soit effectuée ---
particulièrement pour les équations \ref{eq:sal} et \ref{eq:sal_c}[^mincer] ---
la particularité de notre base de données nous a poussé à faire autrement.

En effet, la majorité de nos variables sont dichotomiques; s'il est possible de
les traiter pour les inclure dans une fonction logarithmique, cela aurait
nécessité un traitement statistique supplémentaire et aurait complexifié
l'interprétation des résultats.

De plus, d'aucuns apprécient utiliser des fonctions logarithmiques car elles
permettent d'interpréter les coefficients de régression en termes de
pourcentages. Dans le cas des équations \ref{eq:hob}, \ref{eq:hob_c},
\ref{eq:bel}, \ref{eq:bel_c}, \ref{eq:alt}, \ref{eq:alt_c}, \ref{eq:sat} et
\ref{eq:sat_c}, la valeur de la variable dépendante analysée est par
construction entre 0 et 1. On obtient ainsi le même résultat.

[^mincer]: Il est courant de voir des variables comme le salaire représentées
  sous la forme d'une fonction logarithmique du niveau d'éducation et de
  l'expérience sur le marché du travail (équation de Mincer)
  [@heckman_lochner_todd2003].

### Tests statistiques

#### Colinéarité parfaite

Pour éviter des problèmes de colinéarité parfaite, on exclut des matrices $D_1$
$D_2$, $D_3$, $D_4$, $D_5$, $D_6$, $D_7$, $D_8$, $D_9$ et $D_{10}$ les variables
`Age2534` et `EmploymentFull`, que l'on peut ainsi considérer comme des
situations de référence. La valeur du coefficient de régression de ces
variables peut être obtenue par le théorème de Frisch-Waugh.

#### Endogénéité

Comme nous l'expliquons en détail à la section 2.3, les résultats que nous
obtenons démontrent une corrélation entre les variables dépendantes étudiées et
les deux variables indépendantes qui nous intéressent (`OpenSource` et
`OpenSourceComposite`). Il ne nous est cependant pas possible de parler de
relations causales, notamment pour des questions d'endogénéité.

Dans *A Guide to Modern Econometrics*, Marno Verbeek affirme en effet:

> *[The problem of endogeneity] also arises if there are unobservable omitted
factors in the model that happen to be correlated with one or more of the
explanatory variables. This bias is of particular concern when we wish to
attach a causal interpretation to our model coefficients, in which case the
ceteris paribus condition includes all other factors that have an impact on the
outcome variable $y_i$, whether observed or unobserved.* --- [@verbeek2017,
146]

Il est ainsi pertinent de se demander si nos deux variables d'intérêts sont
exogènes, une condition nécessaire pour conclure l'existence d'une relation
causale.  Sans nous avancer dans l'élaboration d'un modèle complexe pour
expliquer la variable `OpenSource`, on peut raisonnablement conclure que cette
dernière est le résultat d'interactions complexes et variées et que nous avons
un problème de variables omises. Il ne serait pas non plus surprenant qu'un
phénomène de causalité inverse (*reverse causality*) existe entre les variables
`OpenSource` et `ConvertedSalary`, tel qu'avancé dans la section d'analyse des
résultats.

Par construction, la présence d'endogénéité dans la variable `OpenSource`
implique la présence d'endogénéité dans la variable `OpenSourceComposite`.
Notons toutefois que plusieurs variables explicatives dans nos équations
utilisées à fin de contrôle, comme `Age`, `GenderDummy` et `RaceEthnicityDummy`,
sont effectivement exogènes.

Plusieurs techniques existent pour prendre en compte de la présence
d'endogénéité dans nos données, la plus courante étant l'élaboration d'un
modèle à variables instrumentales. Ce genre de modèle permet d'effectuer un
test de Durbin-Wu-Hausman pour vérifier la présence d'endogénéité.
Malheureusement, il nous semble très difficile, voire impossible de procéder de
la sorte: la base de données avec laquelle nous travaillons ne fournit pas les
instruments nécessaires pour réaliser un tel exercice.

Le lecteur ou la lectrice devrait donc être conscient-e de cette problématique
probable lors de la lecture de ce travail et analyser les résultats fournis en
toute connaissance de cause.

Si nous avions pu participer à la collecte de données, il aurait été
intéressant de poser des questions supplémentaires aux répondant-e-s et
d'obtenir des variables permettant d'effectuer les tests précédemment
mentionnés.

Pour obtenir un instrument corrélé avec la participation à un projet libre sans
l'être avec le salaire ou la satisfaction au travail, on aurait ainsi pu leur
demander: « Dans la dernière année, avez-vous recommandé à vos ami-e-s ou à vos
proches l'utilisation de logiciels libres comme alternatives à des logiciels
propriétaires? ».

L'intuition derrière cette question est que recommander un logiciel libre à son
entourage est un bon indicateur que l'on participe à un projet libre ou que
l'on fait parti de la communauté libre. On peut de plus raisonnablement
supposer que ce comportement est indépendant du salaire, de la satisfaction au
travail et des autres variables explicatives utilisées aux équations
\ref{eq:sal} et \ref{eq:sat}.

Pour le modèle présenté à l'équation \ref{eq:bel}, tentant d'évaluer le lien
entre la participation à un projet libre et le sentiment d'appartenance à la
communauté informatique, ou encore celui à l'équation \ref{eq:alt}, traitant de
la propension à l'altruisme, on aurait pu suivre un raisonnement similaire et
poser la question suivante: « Avez-vous déjà utilisé vos contributions à un ou
des projets libres (par exemple sur Gitlab ou Github) comme argument en votre
faveur lors d'un processus d'embauche? ».

Finalement, comme instrument permettant d'utiliser un modèle à variables
instrumentales pour traiter la possibilité d'endogénéité dans la relation entre
la participation à un projet libre et le fait d'écrire du code informatique
comme hobby (équation \ref{eq:hob}), nous aurions pu demander aux
répondant-e-s: « Écrivez-vous du code libre dans le cadre de votre travail à la
demande de votre employeur? ».

#### Hétéroscédasticité

Pour chacune des régressions décrites dans la section 2.2, nous avons effectué
un test de Breusch-Pagan pour vérifier que la variance des termes d'erreurs est
constante. Nous constatons ainsi la présence d'hétéroscédasticité dans nos
données.

Pour pallier à ce problème, les écarts types présentés à l'annexe B ont été
générés à l'aide d'un estimateur de White [-@white1980], où la matrice de
variance-covariance prend la forme suivante:

\begin{equation}
  (X'X)^{-1} X' \Omega X (X'X)^{-1} \notag
\end{equation}

## Analyse des résultats

Dans ce chapitre, nous avions comme but de tester les différentes hypothèses
avancées par le panel d'expert-e-s présenté au second chapitre. Plus
précisément, nous souhaitions vérifier le lien entre la participation à un
projet libre et le salaire, le plaisir ressenti à écrire du code informatique,
le sentiment d'appartenance à la communauté informatique, l'altruisme et la
satisfaction au travail. Peut-on considérer ces facteurs comme des incitatifs
au libre?

En analysant les résultats obtenus au tableau \ref{table:reg_sal}, il semble
qu'il existe bel et bien un lien entre le salaire d'une personne travaillant en
informatique et sa préférence pour les logiciels libres. En effet, le
coefficient de régression estimé pour la variable `OpenSource` à l'équation
\ref{eq:sal} est de 9 101 USD, ce qui représente environ 17\ % du salaire annuel
médian gagné par les individus ayant répondu au sondage. Qui plus est, cette
relation a une valeur-p quasi-nulle et est donc très significative.

On ne peut cependant pas en dire autant pour le coefficient de régression
estimé pour la variable `OpenSourceComposite` à l'équation \ref{eq:sal_c},
présenté dans le tableau \ref{table:reg_sal_c}, ce dernier ayant une valeur-p
supérieure à 0.05, seuil minimal que nous sommes prêts à accepter dans le cadre
de ce mémoire.

Peut-on donc conclure, à l'instar de Lerner et Tirole, qu'en définitive, les
développeuses et les développeurs écrivent des logiciels libres pour gagner un
plus gros salaire? Cela serait à notre avis présomptueux.

En effet, si nous avons prouvé qu'il existe une corrélation forte et
significative entre le salaire et la participation à un projet libre, il ne
nous est pas possible de nous prononcer sur la causalité de ce lien. Tel
qu'argumenté par Angrist et Krueger [-@angrist_krueger1999], une analyse
statistique plus poussée nous permettrait sûrement d'aller plus loin et de
s'aventurer sur le terrain des relations causales; cela dépasse cependant le
cadre de ce mémoire. Lerner et Tirole ont sûrement partiellement raison, mais
il se pourrait par exemple que la relation soit inverse.

Les gens qui gagnent un salaire plus élevé ont généralement plus de latitude
financière et sont plus à même de négocier des clauses dans leur contrat
d'embauche quant à la propriété intellectuelle du code qu'elles et ils écrivent
ou encore de prendre le temps de choisir un emploi qui leur permet d'écrire du
libre au travail. Chose certaine, nos résultats n'infirment pas leurs
hypothèses.

Qu'en est-il des hypothèses de Wu, Gerlarch & Young, résumées par l'équation
\ref{eq:wu_gerlach_young_pref}? Malheureusement pour nous, il semble que notre
base de données soit peu adaptée à valider ou invalider leur modèle.

En effet, s'il est possible d'affirmer que « le désir d’améliorer son capital
humain » et « la perspective d’obtenir un meilleur emploi » peuvent être
représentés par l'instrument `ConvertedSalary`, on commence à tirer
l'élastique en tentant de représenter « la motivation intrinsèque d’aider » par
notre variable `Altruism`. Qui plus est, nous n'avons pas de données pouvant
prétendre mesurer le nombre de répondant-e-s qui écrivent du code libre pour
combler leurs « besoins personnels ».

Attardons-nous tout de même aux résultats des tableaux \ref{table:reg_alt} et
\ref{table:reg_alt_c}, qui présentent respectivement l'effet de la
participation à un projet libre et l'effet du degré de participation à la
communauté libre sur l'altruisme. On constate de fait que les coefficients de
régression estimés pour les variables `OpenSource` (équation \ref{eq:alt}) et
`OpenSourceComposite` (équation \ref{eq:alt_c}) sont fortement significatifs.

Ces coefficients sont toutefois assez faibles, marquant une différence
d'environ 2 points de pourcentage chez les individus affirmant écrire du code
libre et de 4 points de pourcentage pour une personne étant très impliquée dans
la communauté libre. Plutôt que d'en conclure que l'altruisme n'est pas un
incitatif important à l'écriture de code libre, nous préférons plutôt retenir
que la construction de la variable `Altruism` à l'équation \ref{eq:var_alt}
semble problématique.

En effet, comme abordé dans la section dédiée à la construction de cette
variable, l'altruisme est une chose difficile à quantifier empiriquement. Si la
relation mise de l'avant par ces deux équations est certainement réelle, nous
ne sommes toutefois pas prêts à affirmer que la corrélation mesurée représente
l'altruisme comme phénomène social dans toute sa complexité. Pourrait-on plutôt
parler d'une propension éthique marginalement supérieure chez les gens qui
s'impliquent activement dans les communautés libres?

À l'instar de l'altruisme, les coefficients de régression estimés exprimant la
relation entre la participation à un projet libre et le sentiment
d'appartenance à la communauté informatique (tableaux \ref{table:reg_bel} et
\ref{table:reg_bel_c}) sont faibles et très significatifs.  Ainsi, le
coefficient estimé pour la variable `OpenSource` (équation \ref{eq:bel}) est de
0.03 et celui pour la variable `OpenSourceComposite` (équation \ref{eq:bel_c})
est de 0.04.

Ces résultats sont surprenants. À première vue, on aurait pu s'attendre à ce
que la réputation de la communauté libre pour son fort esprit de corps, ainsi
que son attachement à un ethos souvent contraignant, se traduise par un
sentiment d'appartenance élevé. En poussant l'analyse plus loin, on constate
toutefois que seul 8\ % des répondant-e-s affirmaient ne pas ressentir de tel
sentiment. À notre avis, la faiblesse des coefficients estimés peut être
expliquée par le fort sentiment d'appartenance présent dans l'ensemble de la
profession informatique, libre ou non.

Le troisième modèle que nous avons analysé au chapitre deux est celui de
Yochai Benkler, que nous avons résumé à l'équation \ref{eq:benkler_utility}. En
reprenant les termes utilisés par Benkler, on peut ainsi associer « l'utilité
de la récompense monétaire » à la variable `ConvertedSalary`, « l'utilité de la
récompense hédoniste intrinsèque » aux variables `Hobby` et `Altruism`, et
finalement, « l'utilité de la récompense socio-psychologique » à la variable
`Belonging`. Seuls les résultats concernant la variable `Hobby` n'ont pas déjà
été analysés plus haut.

Comme on peut l'observer aux tableaux \ref{table:reg_hob} et
\ref{table:reg_hob_c}, les coefficients de régression estimés décrivant la
relation entre la participation à un projet libre et le plaisir d'écrire du
code informatique sont très significatifs. Il semble ainsi qu'une personne qui
participe à un projet libre (variable `OpenSource` dans l'équation
\ref{eq:hob}) voit ses chances d'écrire du code informatique comme hobby
augmenter de 17 points de pourcentage. Pour une personne très fortement
impliquée dans la communauté libre (variable `OpenSourceComposite` dans
l'équation \ref{eq:hob_c}), cette augmentation se chiffre à 23 points de
pourcentage.

Nos résultats empiriques semblent donc supporter la relation décrite à
l'équation \ref{eq:benkler_utility}. Pour en dire plus, il serait à notre avis
nécessaire d'effectuer une analyse plus profonde des implications de ce modèle.
Il serait ainsi intéressant de bâtir dans un travail subséquent un vrai modèle
expérimental en tentant d'identifier les variables composant $M$, $H$ et $S$ et
de tester son pouvoir de prédiction.

La seule relation que nous n'avons pas encore analysée est celle reliant la
participation à la communauté libre et la satisfaction au travail. Présentés
aux tableaux \ref{table:reg_sat} et \ref{table:reg_sat_c}, les résultats
obtenus sont relativement décevants. En effet, si les coefficients de
régression estimés pour les variables `OpenSource` (équation \ref{eq:sat}) et
`OpenSourceComposite` (équation \ref{eq:sat_c}) sont fortement significatifs,
les valeurs obtenues sont très faibles.

Avec un peu de recul, ces résultats ont du sens. En effet, si nous connaissons
le degré de satisfaction au travail des répondant-e-s, nous ne savons pas si
elles et ils écrivent du code libre au travail. Il est ainsi peu probable
qu'une personne fortement impliquée dans la communauté libre mais forcée
d'écrire du code propriétaire pour gagner sa vie soit très satisfaite de son
emploi. À l'inverse, ont peut facilement imaginer la frustration d'une personne
qui, obligée par son employeur à écrire du libre, n'en voit pas l'intérêt. Il
nous aurait malheureusement fallu une variable supplémentaire pour être en
mesure de tirer des conclusions intéressantes de cette relation.

[modeline]: # ( vim: set spl=fr: )
