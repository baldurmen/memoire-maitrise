# Conclusion {-}

Au cours de ce travail, nous avons abordé plusieurs questions importantes
quant à la structure d'incitatifs économiques liés aux logiciels libres. Si au
premier abord la maintenance collective de communs numériques libres peut
sembler -- du point de vue de l'économie néoclassique -- vouée à l'échec, nous
avons démontré que d'autres approches sont à même d'expliquer ce phénomène.
Ainsi, les travaux d'Elinor Ostrom sur la gestion collective des communs
semblent à notre avis décrire le modèle de développement propre aux logiciels
libres avec beaucoup d'acuité.

Nous nous sommes par la suite attardés à l'étude de travaux économiques tentant
de modéliser les incitatifs à l'écriture de code libre. Nous avons ainsi pu
constater que ces derniers sont généralement peu complexes et ont rarement été
vérifiés à l'aide de données empiriques valides. Si certains modèles, comme
celui présenté par Lerner et Tirole, mettent l'accent sur l'espérance de gain
monétaire comme incitatif principal, nous leur avons préféré des modèles plus
complexes, prenant également en compte des facteurs socio-psychologiques et
faisant la distinction entre incitatifs intrinsèques et extrinsèques. En
définitive, la nature très hétérogène des projets libres complexifie
l'élaboration d'un modèle unique et détaillé.

Pour tenter d'y voir plus clair, nous avons finalement analysé de manière
empirique les hypothèses à la base de ces modèles grâce aux données du *Stack
Overflow Developers Survey 2018*, un sondage exhaustif ayant été rempli par
plus de 100 000 développeuses et développeurs. Nous avons de fait découvert une
corrélation forte et significative entre la participation à un projet libre et
un salaire plus élevé. De même, il semble exister une corrélation forte et
significative entre la participation à un projet libre et le plaisir d'écrire
du code informatique. Nous avons également trouvé qu'à un plus faible degré, la
participation à un projet libre est corrélée positivement avec le sentiment
d’appartenance à la communauté informatique, l'altruisme et la satisfaction au
travail. Ces résultats doivent cependant être analysés en prenant en compte la
présence possible d'endogénéité dans nos équations.

Comme nous l'avons présenté dans le second chapitre, la question des
incitatifs économiques au libre est très souvent associée à la nécessité de
trouver des sources de financement pérennes pour développer les communs
numériques. Nous avons donc choisi de présenter différents modèles d'affaires
populaires dans l'écosystème libre à l'annexe A en guise de complément.

Sans être révolutionnaire, nous sommes d'avis que ce travail apporte certaines
réponses à la question « Qu'est-ce qui motive une personne à écrire des
logiciels libres? ». Comme abordé à la section 2.3, la prochaine étape serait
de bâtir un vrai modèle détaillé d'incitatifs économiques et sociaux.

Malheureusement, le désintérêt des économistes pour les rouages de l'écosystème
libre depuis la fin des années 2000 est marqué. Cela est regrettable, car comme
nous avons su le prouver dans ce travail, les données pour traiter de cette
question existent et sont de qualité acceptable. Si le sondage annuel de Stack
Overflow n'a pas la prétention d'être une étude longitudinale, les questions
qui nous intéressent le plus comme le salaire annuel et la participation à un
projet libre reviennent années après années. Un-e économiste s'intéressant au
libre trouverait ici de quoi s'occuper pour nombreuses années à venir.

[modeline]: # ( vim: set spl=fr: )
