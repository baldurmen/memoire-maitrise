# Annexe A: Quels modèles d'affaires pour financer les logiciels libres? {-}

Comme nous l'avons abordé dans le premier chapitre de ce mémoire,
l'informatique libre est très hétérogène et répond à des besoins vastes et
variés. Ainsi, un logiciel comme le serveur web *Apache* --- dont dépend une
bonne partie du trafic en ligne --- n'a pas la même structure de développement
qu'une application libre destinée aux téléphones intelligents, ou encore qu'un
script libre utilisé par une personne faisant de l'administration système.

Il existe conséquemment de nombreux modèles d'affaires qui permettent de
financer le développement de logiciels libres. Dans cette annexe, nous
souhaitons recenser les plus communs et donner quelques exemples pertinents de
réussites (et parfois d'échecs) de ces modèles.

## Financement académique {-}

Avant l'apparition des premières licences libres, une partie importante des
logiciels distribués librement l'étaient par les communautés *hackers*, liées
de près ou de loin au milieu académique. Les logiciels écrits par ces
communautés sont les précurseurs des logiciels libres que l'on connait de nos
jours.

Le financement académique joue encore un rôle important dans les communautés
libres. Si plusieurs universités supportent activement le développement de
certains logiciels au niveau institutionnel (dons, contrats de support,
contributions explicites par des employé-e-s), une partie non négligeable de
l'apport du milieu académique aux logiciels libres provient du travail effectué
par des chercheuses, des chercheurs et des professeur-e-s.

En effet, l'indépendance académique et la structure salariale très stable des
professeur-e-s leur permet de contribuer plus facilement aux communs libres que
d'autres corps de métiers. Il existe de surcroît d'importants rapprochements
entre l'ethos scientifique [@merton1973] et l'ethos du mouvement libre. Cela
s'exprime entre autre par la synergie de ces deux mouvements dans la Science
ouverte [@benkler2002, 381].

Certains langages de programmation comme *OCaml* sont même réputés pour être
très « académiques » et ne sont que faiblement utilisés en dehors des
laboratoires et centres de recherche universitaires. Sans le financement et le
support du milieu académique, la viabilité économique d'importants projets à
faible valeur commerciale comme le *Software Heritage* de l'INRIA
[@dicosmo_zacchiroli2017] --- un projet d'archive universelle des logiciels
libres --- serait carrément impossible.

La popularité de certains outils libres dans la communauté académique n'est
toutefois pas forcément gage de leur soutenabilité. En effet, plusieurs
programmes et bibliothèques (*libraries*) à caractère scientifique très
utilisées comme *Matplotlib* ont d'importantes difficultés à lever des fonds
académiques pour assurer leur avenir:

> *On 10 April [2019], astrophysicists announced that they had captured the
first ever image of a black hole. This was exhilarating news, but none of the
giddy headlines mentioned that the image would have been impossible without
open-source software. The image was created using Matplotlib, a Python library
for graphing data, as well as other components of the open-source Python
ecosystem. Just five days later, the US National Science Foundation (NSF)
rejected a grant proposal to support that ecosystem, saying that the software
lacked sufficient impact.* --- [@nowogrodzki2019]

## Financement gouvernemental {-}

Comme toutes les grandes organisations, les gouvernements ont des besoins
technologiques qui peuvent parfois être complexes. De plus --- contrairement à
certaines entreprises --- les États doivent typiquement respecter des
protocoles de sécurité très contraignants tout en évitant de se placer à la
merci d'entreprises privées. Cela est particulièrement vrai dans l'armée.

Beaucoup de gouvernements se tournent donc vers des solutions libres, car ces
dernières ont l'avantage de ne pas êtres susceptibles à l'enfermement
propriétaire (*Vendor Lock-in*), de permettre la mutualisation des coûts de
développement avec d'autres organismes publics et de faciliter les audits de
sécurité indépendants. C'est par exemple le choix qu'à fait la France, où la
Gendarmerie nationale a décidé en 2013 de migrer plus de 30 000 postes
informatiques à Linux [@elie2019]. De manière similaire, le gouvernement de
Taïwan a décidé de se tourner vers les logiciels libres pour appuyer ses
démarches de renouveau démocratique [@oflaherty2018; @tang2018].

Des logiciels libres comme le projet Tor, permettant de s'anonymiser en ligne,
ou encore *LibreOffice*, une suite de logiciels de bureautique libre rivalisant
*Microsoft Office*, sont ainsi partiellement financés par différents organismes
gouvernementaux à travers le monde [@otf2017]. Certains pays financent même
directement des projets libres à partir de fonds spéciaux, comme le fait
l'Allemagne avec son *Prototype Fund*.

L'adoption des logiciels libres dans la fonction publique --- et donc leur
financement par ces organismes --- est cependant liée de très près à la volonté
des gouvernements élus. Un changement de gouvernement municipal en 2017 dans la
ville de Munich a ainsi engendré la décommission des 13 000 postes de travail
Linux au profit de machines Windows [@tomazeli2019]. Munich était pourtant
considérée depuis 2004 comme une pionnière des logiciels libres dans la
fonction publique et contribuait activement à certains projets libres.

Face à ces défis, plusieurs groupes de pression organisent des campagnes de
mobilisation pour défendre la place des logiciels libres dans les organismes
publics. Ainsi, la campagne européenne *Public Money, Public Code*, visant à
s'assurer que les logiciels développés avec de l'argent public soient
systématiquement publiés sous des licences libres, joue actuellement un rôle
important dans la promotion du libre en Europe [@moody2019]. Fait surprenant,
depuis 1895 le travail produit par les agences gouvernementales fédérales aux
États-Unis (code informatique, photos, œuvres littéraires, etc.) ne sont pas
protégées par le copyright et appartiennent au domaine public [@usa1961;
@ieee1977].

## Fondations {-}

Parfois, certains projets n'ayant pas de visées commerciales deviennent
tellement importants qu'il leur est nécessaire d'engager du personnel pour
faire du travail administratif. En général, ces projets établissent une
fondation qui s'incorpore comme organisme de bienfaisance, lui permettant ainsi
de récolter des dons et d'émettre des reçus déductibles d'impôt. C'est par
exemple le cas de la *Document Foundation*, qui maintient la suite
*LibreOffice,* la fondation *Apache*, qui maintient entre autres le serveur web
*Apache*, et la fondation Linux, qui travaille entre autres sur le noyau
Linux.

La plupart du temps, les décisions techniques du projet et les décisions
financières de la fondation sont indépendantes. Ainsi, un projet s'assure de sa
pérennité financière tout en conservant sa liberté technique. La fondation est
alors responsable d'engager des contributrices et des contributeurs à contrat
pour travailler sur le projet.

Ce modèle d'affaires repose en très grande partie sur des dons effectués par
des entreprises, des gouvernements et des particuliers. Si ces groupes ont
souvent à cœur le développement des programmes chapeautés par ces fondations,
les entreprises voient également en ces dons une manière d'améliorer leur image
de marque et de payer moins d'impôt.

Plusieurs grandes entreprises disposent également de fondations privées qui
financent parfois des projets libres. L'impact de ces fondations est cependant
relativement limité et ne pourrait pas réellement être considéré comme un modèle
d'affaires à proprement parler.

## Socio-financement {-}

Phénomène relativement récent --- particulièrement dans le monde du libre ---
le socio-financement se distingue d'autres modèles d'affaires par sa volonté de
faire reposer les coûts du développement d'un logiciel sur les individus qui
l'utilisent.

Les campagnes de socio-financement fructueuses visent souvent à soutenir une
personne travaillant sur de multiples projets libres plutôt qu'un projet en
particulier. Les revenus tirés du socio-financement sont typiquement faibles et
ont habituellement comme but premier la diversification des sources de revenu
d'un développeur ou d'une développeuse.

Si quelques individus ayant une certaine crédibilité dans leurs communautés
respectives comme Joey Hess [-@hess2018] ou encore Drew DeVault [-@devault2019]
s'en tirent somme toute relativement bien, ces cas font figures d'exception.
Plusieurs, comme François Élie [-@elie2019], considèrent cependant le
socio-financement comme une source de financement peu efficace et parfois même
dommageable, préférant plutôt que les coûts de développement des logiciels
libres reposent sur des institutions et non sur des individus.

## Licences multiples {-}

Comme nous l'avons vu dans l'introduction de ce mémoire, ce qui différencie le
code libre du code propriétaire est sa licence de propriété intellectuelle. Il
arrive cependant qu'un programme soit simultanément publié sous une licence
libre et une licence propriétaire. On parle alors de projet à licences
multiples (*Multi-licensing*).

La licence d'un projet libre peut parfois être considérée comme un frein à son
adoption par une grande entreprise pour des motifs légaux, les services de
conseils juridiques étant reconnus pour leur faible enthousiasme envers les
logiciels libres. D'aucuns souhaiteraient également intégrer à des projets
propriétaires du code libre normalement publié sous une licence restrictive
comme la GNU GPL.

Utiliser de multiples licences permet donc à ces projets de vendre des licences
propriétaires à des prix leur permettant de financer leurs activités. En
général, les projets ayant des licences multiples ont des licences principales
très restrictives pour inciter les entreprises à acheter une licence
propriétaire. C'est par exemple le cas pour l'infrastructure de développement
*Qt* ou encore pour le serveur de téléphonie numérique *Asterisk*.

## Vente de support {-}

La vente de support commercial pour financer un projet libre est un modèle
populaire et ayant fait ses preuves, comme peut en témoigner l'énorme succès de
Red Hat [@levine2014]. Certaines entreprises sont attirées par les différentes
vertus des logiciels libres (dont le fait d'éviter l'enfermement propriétaire),
mais n'ont pas forcément les ressources à l'interne pour s'assurer du bon
fonctionnement du logiciel en question. D'autres préfèrent tout simplement
contracter de l'aide extérieure et se concentrer sur leur mission principale.

Ces entreprises font donc affaire avec un partenaire externe offrant du support
commercial pour le logiciel qu'elles décident d'utiliser. Souvent, ce
partenaire est l'entité principale qui développe le logiciel. Néanmoins, la
nature ouverte des logiciels libres fait en sorte qu'il existe parfois
plusieurs entités commerciales collaborant au développement du même logiciel.

Ce type de modèle fonctionne relativement bien car il constitue un modèle
d'affaires clair, qui permet de monter un plan d'affaires traditionnel. De plus,
comme ce type d'offre existe déjà pour de nombreux logiciels propriétaires, les
entreprises sont généralement à l'aise avec ce genre de dépenses.

Certains affirment toutefois qu'il peut être difficile de convaincre des
entreprises de payer pour du support lorsqu'un logiciel leur est offert à un
coût monétaire nul [@janke2010-06; @janke2010-07-26; @janke2010-07-12;
@akhmechet2017].

## Logiciel en tant que service {-}

Grâce à l'émergence de plateformes d'infonuagique (*Cloud Computing*)
compétitives, robustes et peu onéreuses, la vente de logiciels en tant que
service (*Software as a Service* --- SaaS) est un modèle d'affaires de plus en
plus populaire. Poussant le concept de vente de support un cran plus loin, ce
modèle d'affaires propose aux entreprises d'héberger et de gérer un logiciel
libre pour elles, en échange d'un montant mensuel ou annuel.

Historiquement, ce modèle était réservé aux grands projets libres bien établis:
pour être en mesure d'offrir un produit en SaaS compétitif dans les années 2000,
il était nécessaire d'acheter et de maintenir des serveur physiques dans un
centre de données --- une entreprise complexe, coûteuse et risquée.

Avec l'arrivée d'offres de location de ressources en centre de données très
compétitives (*Amazon Web Services*, *Microsoft Azure*, *Google Cloud
Platform*, etc.), les entreprises souhaitant offrir du SaaS n'ont plus besoin
de faire des investissements en capitaux importants et peuvent moduler leurs
coûts fixes en fonction de la demande, parfois de manière très agressive.

Comme pour la vente de support, le SaaS n'est pas un modèle d'affaires unique
aux logiciels libres et est également très populaire dans le monde des
logiciels propriétaires, créant ainsi un sentiment de familiarité pour les
entreprises qui décident de retenir ce genre de service.

Plusieurs projets de technologies web libres comme *Wordpress* [@mullenweg2014]
tirent une majorité de leurs revenus du SaaS.

## Travail bénévole {-}

Le travail bénévole est de loin le «modèle d'affaires» le plus populaire pour le
développement de logiciels libres. L'écrasante majorité des projets libres
reposent d'une manière ou d'une autre sur une certaine proportion de travail non
rémunéré, que ce soit par la contribution directe de code ou encore par
l'écriture de rapports de bogues.

## Chasseurs de prime {-}

Populaire surtout dans le milieu de la sécurité informatique, certaines grandes
organisations offrent des primes (parfois importantes) pour la découverte de
problèmes de sécurité majeurs pour certains logiciels libres jugés critiques
[@reda2019].

Il est rare qu'un projet ne survive que par les gains faits à travers des primes
de ce genre, mais certaines personnes sont en mesure de financer leur travail
sur des projets libres en récoltant ces primes.

## Open Core {-}

Le modèle *Open Core* consiste à financer le développement d'un logiciel libre
(le *core*) en vendant des fonctionnalités propriétaires additionnelles.
Souvent ces fonctionnalités sont nécessaires pour déployer le logiciel en
entreprise. Historiquement très répandu dans la communauté libre, ce modèle a
perdu en popularité au fil des années au profit de modèles jugés «plus modernes»
[@dix2017], comme le SaaS. Malgré tout, de nombreux géants du libre, comme
Gitlab, Puppet ou encore ElasticSearch, utilisent ce modèle d'affaires avec
beaucoup de succès.

De nombreuses personnes soutiennent cependant que le modèle *Open Core* est
contraire à l'ethos des logiciels libres [@prentice2010; @kuhn2009;
@phipps2010]. Ces personnes affirment en effet que financer le développement de
ces logiciels grâce à l'érection de barrières artificielles visant à
restreindre l'accès à certaines parties du code informatique n'est pas
conséquent avec les valeurs à la base du mouvement libre.

## Mutualisation {-}

Modèle plus large englobant souvent de nombreuses sources de financement
diverses, la mutualisation permet à différentes entités de se regrouper et de
diviser les coûts de maintenance et de développement d'un projet.

Plusieurs types d'entités utilisent la mutualisation comme moyen de financer
des projets complexes. Par exemple, le projet *Civil Infrastructure Platform*
regroupe des partenaires comme Siemens, Toshiba et Renesas Electronics dans le
but de supporter la maintenance de versions spécifiques du noyau Linux sur le
très long terme [^SLTS] pour assurer la pérennité d'infrastructures civiles
critiques dans le secteur du transport et de la production d'énergie
[@kobayashi2017].

En France, la mutualisation permet à de nombreuses municipalités de petite
taille de s'associer pour développer des logiciels adaptés à leurs réalités.
Fondée en 2002, l'association des développeurs et utilisateurs de logiciels
libres pour les administrations et les collectivités territoriales (ADULACT)
offre ainsi à ses membres un marché commun ainsi qu'une forge logicielle
[@elie2019].

Au Canada la seule initiative majeure de mutualisation des infrastructures
informatiques libres est le projet *Open Ressource Exchange* --- géré par le
gouvernement fédéral --- et qui vise à mettre en lien différents acteurs
gouvernementaux (agences fédérales, provinces et municipalités) à travers le
pays pour développer et maintenir collectivement des solutions informatiques
partagées [@tomazeli2019].

[^SLTS]: Les projets d'ingénierie civile dont les membres de la *Civil
  Infrastructure Platform* ont la charge ont une durée de vie typique allant de
  25 à 50 ans --- une véritable éternité en informatique.

[modeline]: # ( vim: set spl=fr: )
