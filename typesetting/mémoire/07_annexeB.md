# Annexe B: Tableaux statistiques {-}

\input{../tables/aggregates.tex}
\input{../tables/reg_sal.tex}
\input{../tables/reg_sal_c.tex}
\input{../tables/reg_hob.tex}
\input{../tables/reg_hob_c.tex}
\input{../tables/reg_bel.tex}
\input{../tables/reg_bel_c.tex}
\input{../tables/reg_alt.tex}
\input{../tables/reg_alt_c.tex}
\input{../tables/reg_sat.tex}
\input{../tables/reg_sat_c.tex}

[modeline]: # ( vim: set spl=fr: )
